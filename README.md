# README #

Music.local é uma aplicação que permite reproduzir as músicas do seu PC em um dispositivo Android através da rede local. 

## Overview ##

Music.local foi projetada para ser utilizada no escopo de uma rede doméstica. Esta aplicação é composta de duas entidades: um cliente e um servidor. É no servidor onde estão armazenados os arquivos de música. Os clientes realizam o download e reprodução destes arquivos.

O servidor, desenvolvido em Java (utilizando [Jetty](http://www.eclipse.org/jetty/)), segue os padrões [RESTful](https://pt.wikipedia.org/wiki/REST) para prover acesso à coleção de músicas de um usuário. Os clientes, desenvolvidos para Android, podem visualizar os servidores disponíveis na rede e, para cada um deles, quais as músicas estão sendo ofertadas.

### Zeroconf ###

[Zeroconf (Zero-Configuration Network)](http://www.zeroconf.org/) consiste em um conjunto de tecnologias (e protocolos) para permitir a conexão entre máquinas, bem como a descoberta e divulgação de serviços, em uma rede IP com mínima ou nenhuma configuração e sem necessitar de serviços externos (ex.: servidores DHCP ou DNS).

Music.local utiliza uma implementação em Java, [JmDNS](http://sourceforge.net/projects/jmdns/), dos protocolos presentes na Zeroconf: [DNS-SD (DNS Service Discovery)](http://www.dns-sd.org/) e Multicast DNS. Isto permite que clientes descubram e utilizem os servidores presentes na rede local sem a necessidade de configurar ou instalar serviços DNS. Da mesma forma, qualquer usuário na rede pode executar um ou mais servidores para prover acesso às suas músicas.

## Uso ##

É possível fazer o download dos arquivos executáveis na [página de downloads](https://bitbucket.org/paulobrizolara/music.local/downloads) ou compilar o projeto manualmente (ver abaixo).

O sistema consiste em dois executáveis: o servidor e o cliente. 
O servidor é uma aplicação de linha de comando distribuída como um '.jar'. Para executá-lo é possível utilizar o seguinte comando:

    java -jar MusicLocalServer.jar
    
As seguintes opções podem ser utilizadas no servidor:

     -d,--musicdir <arg>   Define a pasta raiz de músicas que será utilizada. Adiciona recursivamente 
                           todas as músicas  encontradas a partir deste diretório.
                           Se não definida, utiliza todas as músicas conhecidas.
                           
     -h,--help             Imprime a mensagem de ajuda e finaliza.
     
     -n,--name <arg>       Define o nome que o servidor utilizará para se anunciar aos clientes. 
                           Se não for definida, um nome padrão é utilizado.
                           
     -p,--port <arg>       Define a porta em que o servidor será executado. Se não for definida, será 
                           utilizada uma porta dinâmica (definida pelo sistema operacional).

Para finalizar o servidor basta digitar Enter no terminal.
                           
O cliente Android consiste em um aplicativo ".apk". Para instalá-lo:

* Copie o arquivo para o dispositivo Android
* Abra a pasta do arquivo (utilizando o gerenciador de arquivos)
* Toque no arquivo .apk
* Selecione a opção para instalar o arquivo (ex.: "Instalador do pacote")
* Aceite a instalação (se necessário)

### Compilando ###

Antes de compilar o programa verifique que os pré-requisitos estão instalados:

* JDK 7 ou superior
* Android  SDK
    * > Foi utilizada a versão 21 da SDK
* Eclipse IDE (também é possível compilar o projeto utilizando a linha de comando, mas não descreverei aqui as etapas para isto)
* Plugin ADT
* Bibliotecas de compatibilidade do Android
    * > O Eclipse gera um projeto (appcompat_v7) que deve estar incluso no workspace


Após instalados os pré-requisitos, copie ou clone o projeto.

#### Importando para a Eclipse IDE ####

Para importar o projeto do servidor no eclipse, utilize o menu: 

    File > Import
   
Em seguida na guia "General" selecione "Existing Projects into Workspace" e click Next.
Selecione o diretório do projeto (clique em Browse, em seguida navegue até a raiz do projeto e selecione "Ok").
Finalize a importação (click Finish).

Importar o projeto Android é similar, porém na seleção do tipo de importação utilize "Existing Android Code Into Workspace" (na guia "Android").

## Problemas Conhecidos##

Este sistema ainda está em um estado inicial de desenvolvimento, então alguns problemas podem ser encontrados ao utilizá-lo.

Em particular, em alguns momentos pode ocorrer dos clientes não listarem corretamente os servidores. Uma solução alternativa para isto é desconectar e reconectar o cliente e o servidor da rede. 

## Bibliotecas Utilizadas ##

* [Android Asynchronous Http Client](http://loopj.com/android-async-http/) - Cliente Http assíncrono para Android.
* [Apache Commons CLI](http://commons.apache.org/proper/commons-cli/) - tratamento de argumentos da linha de comando.
* [Apache Commons Lang](http://commons.apache.org/proper/commons-lang/) - utilitários.
* [Apache Commons IO](http://commons.apache.org/proper/commons-io/) - manipulação de arquivos.
* [Jackson](https://github.com/FasterXML/jackson) - conversão de objetos para JSON (e vice-versa).
* [Jetty](http://www.eclipse.org/jetty/) - Servidor HTTP embutido.
* [JmDNS](http://sourceforge.net/projects/jmdns/) - Implementação de Multicast DNS e DNS-SD em Java.
* [SQLite JDBC](https://github.com/xerial/sqlite-jdbc) - Manipulação de banco de dados (SQLite) em Java.

## Créditos e Licença ##

>Este projeto foi desenvolvido por Paulo Brizolara como parte de um trabalho de conclusão de curso no Instituto 
>Federal do Rio Grande do Norte (IFRN), sob orientação do profº Alysson Soares.

Music.local é distribuído sob a licença Apache 2.0 (ver arquivo LICENSE.txt). Mais detalhes desta licença podem ser encontrados em: <http://www.apache.org/licenses/>.