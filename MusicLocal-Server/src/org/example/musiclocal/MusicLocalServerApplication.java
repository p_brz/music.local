package org.example.musiclocal;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import javax.jmdns.ServiceInfo;

import org.apache.commons.cli.ParseException;
import org.apache.commons.lang3.StringUtils;
import org.example.musiclocal.OptionsManager.Option;

public class MusicLocalServerApplication {
    /**
     * @param args
     *            the command line arguments
     */
    public static void main(String[] args) {
    	MusicLocalServerApplication application = new MusicLocalServerApplication();
        try {
        	if(application.setup(args)){
        		application.run();
        	}
        }
        catch (Exception e) {
			System.err.println("Ocorreu um erro inesperado, a aplicação será finalizada.");
            e.printStackTrace();
        }
        finally{
        	application.finish();
        }
    }
    
    private static final String DEFAULT_SERVICE_NAME = "Music Server";
	public final static String SERVICE_TYPE = "_musiclocal._tcp.local.";

	private MusicServer musicServer;
	private OptionsManager optionsManager;
	
	public MusicLocalServerApplication() {
		optionsManager = new OptionsManager();
	}
	
	public boolean setup(String[] args) throws IOException {
		try {
			optionsManager.setup(args);
			if(optionsManager.containsOption(Option.Help)){
				optionsManager.printHelpMessage();
				return false;
			}
		} catch (ParseException e) {
			System.err.println("Um erro ocorreu ao tentar interpretar os argumentos fornecidos.");
			e.printStackTrace();
			System.out.println("Utilize: ");
			optionsManager.printHelpMessage();
			
			return false;
		}
		setupMusics();
    	this.musicServer = setupServer(args);
    	
    	return true;
	}
	public void run() throws IOException {
        waitExitCommand();
	}
	public void finish() {
        try {
			teardownServer(musicServer);
	        System.out.println("Done!");
		} catch (IOException e) {
			System.err.println("Failed when trying to finalize application.");
			e.printStackTrace();
		}
	}

	private void waitExitCommand() throws IOException {
		//Block until receive character
		System.out.println("Press Enter to quit");
		System.in.read();
	}

	private MusicServer setupServer(String[] args) throws IOException {
		int port = 0;
		if(optionsManager.containsOption(Option.ServerPort)){
			String portValue = optionsManager.getOptionValue(Option.ServerPort);
			if(portValue != null){
				port = Integer.valueOf(portValue);
			}
		}
		MusicServer musicServer;
		musicServer = new MusicServer(port);
		musicServer.start();
		
		List<Integer> ports = musicServer.getPorts();		
		assertExistPort(ports);
		registerService(ports);
		
		return musicServer;
	}

	private void setupMusics() {
		String musicDir = optionsManager.getOptionValue(OptionsManager.Option.MusicDir);
		if(musicDir != null){
			MusicManager.instance().setMusicRoot(new File(musicDir));
		}
	}

	private void registerService(List<Integer> ports) throws IOException {
		ServiceDiscover.instance().start();
		ServiceInfo serviceInfo = ServiceDiscover.instance().publishService(
				SERVICE_TYPE, getServiceName(), ports.get(0), "");
		System.out.println("\nRegistered Service as " + serviceInfo);
	}

	private String getServiceName() {
		String serviceName = optionsManager.getOptionValue(OptionsManager.Option.ServiceName);
		if(serviceName == null){
			serviceName = getDefaultServiceName();
		}
		return serviceName;
	}

	private String getDefaultServiceName() {
		String hostname = getHostname();
		System.out.println(getClass().getName() + ": " + "Hostname: " + hostname);
		return hostname != null && !hostname.isEmpty() 
												? hostname + " " + DEFAULT_SERVICE_NAME 
												: DEFAULT_SERVICE_NAME;
	}

	private String getHostname() {
		try {
		    String result = InetAddress.getLocalHost().getHostName();
		    if (StringUtils.isNotEmpty( result)){
		        return result;
		    }
		} catch (UnknownHostException e) {
			String host = System.getenv("COMPUTERNAME");
			if (host != null){
			    return host;
			}
			host = System.getenv("HOSTNAME");
			if (host != null){
			    return host;
			}
		}
		
		return null;
	}

	private void assertExistPort(List<Integer> ports) {
		if(ports.size() == 0){
			throw new IllegalStateException("Server has no port!");
		}
	}

	private void teardownServer(MusicServer musicServer) throws IOException {
		if(musicServer != null){
			musicServer.stop();
		}
		ServiceDiscover.instance().stop();
	}
    


	
}
