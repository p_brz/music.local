package org.example.musiclocal;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Music implements Cloneable{
	private long id;
	private String filename;
	private Path filePath;
	private Long lastModifiedTimestamp;
	
	public Music() {
		this(null);
	}
	public Music(File file) {
		this(-1, file);
	}
	public Music(long id, File file) {
		this.id = id;
		this.filename = file == null ? "" : file.getName();
		this.filePath = file == null ? null : file.toPath();
		this.lastModifiedTimestamp = file == null ? null : file.lastModified();
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	@JsonIgnore
	public Path getFilePath() {
		return filePath;
	}
	@JsonIgnore
	public void setFilePath(Path filePath) {
		this.filePath = filePath;
	}
	@Override
	public Object clone() {
		return new Music(this.id, this.filePath == null ? null : filePath.toFile());
	}
	public void setLastModifiedTimestamp(Long lastModified) {
		this.lastModifiedTimestamp = lastModified;
	}
	public Long getLastModifiedTimestamp() {
		return lastModifiedTimestamp;
	}
	@JsonIgnore
	public String getFilePathString() {
		try {
			return this.filePath == null? "" : filePath.toFile().getCanonicalPath();
		} catch (IOException e) {
			return filePath.toAbsolutePath().toString();
		}
	}
	
	@Override
	public String toString() {
		return "{" 
					+ "id:" + this.id 
					+ "; filepath: " + getFilePathString() 
					+ "; lastModified: " + getLastModifiedTimestamp() + "}";
	}
}
