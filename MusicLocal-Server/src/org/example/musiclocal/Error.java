package org.example.musiclocal;

public class Error{
	private int errorCode;
	private String message;
	
	public Error() {
		this(0, "");
	}
	public Error(int errorCode, String message) {
		super();
		this.errorCode = errorCode;
		this.message = message;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	
}