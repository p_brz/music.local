package org.example.musiclocal;


import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CommandListServlet extends HttpServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8125792675032857678L;
	
	private final Map<String, Command> commandList;
	
	public CommandListServlet() {
		commandList = new HashMap<String, Command>();
	}

	public void addCommand(Command command){
		this.commandList.put(command.getName(), command);
	}
	public void removeCommand(Command command){
		this.removeCommand(command.getName());
	}
	public void removeCommand(String commandName){
		this.commandList.remove(commandName);
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_OK);
        
        StringBuilder responseMsg = new StringBuilder();
        responseMsg.append("{\n");
        boolean first = true;
        for(String command : commandList.keySet()){
            responseMsg.append("\t");
            if(!first){
                responseMsg.append(", ");
            }
            else{
            	first = false;
            }
            
            responseMsg.append('"').append(command).append('"').append(" : ")
            			.append("\"").append(commandList.get(command).getUrl()).append("\"")
            			.append("\n");
        }
        responseMsg.append("\n").append("}");
        
        response.getWriter().println(responseMsg.toString());
	}
}