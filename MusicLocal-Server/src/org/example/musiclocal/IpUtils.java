package org.example.musiclocal;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;


public class IpUtils {
	/**
     * Get all IP address of non-localhost interface
     * @return  list of addresses
     * 
     * Adaptado de: http://stackoverflow.com/a/13007325
     */
    public static Collection<InetAddress> getIPAddresses() {

        List<NetworkInterface> interfaces;
		try {
			interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
        List<InetAddress> addrs = new ArrayList<InetAddress>();
        for (NetworkInterface intf : interfaces) {
        	Enumeration<InetAddress> ips = intf.getInetAddresses();
            while (ips.hasMoreElements()) {
                InetAddress addr = ips.nextElement();
            	if (!addr.isLoopbackAddress()) {
                    addrs.add(addr);
                }
            }
        }
        return addrs;
    }
    public static InetAddress getIPv4Address() {
    	Collection<InetAddress> ips = getIPAddresses();
    	
    	for(InetAddress addr : ips){
    		if(addr instanceof Inet4Address){
    			return addr;
    		}
    	}
    	
        return null;
    }
}
