package org.example.musiclocal;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MusicServlet extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8893309354385982118L;
	private static final String GET_FILE_SUBPATH = "file";
	private static final int BUFSIZE = 1024 * 1024; //1MB
	private final ObjectMapper objectMapper;
	public MusicServlet() {
		super();
		objectMapper = new ObjectMapper();
		objectMapper.enable(com.fasterxml.jackson.databind.SerializationFeature.INDENT_OUTPUT);
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("Req pathInfo: " + req.getPathInfo()
				+ "; requestURI: " + req.getRequestURI()
				+ "; servletPath: " + req.getServletPath());
		resp.setCharacterEncoding("UTF-8");
		resp.setStatus(HttpServletResponse.SC_OK);
		resp.setContentType("application/json; encoding=UTF-8");
		
		String pathInfo = req.getPathInfo();
		if(pathInfo == null){
			sendMusicList(resp);
		}
		else{
			String paths[] = StringUtils.split(pathInfo, "/");
			if(paths.length > 0 && StringUtils.isNumeric(paths[0])){
				int musicId = Integer.parseInt(paths[0]);
				sendMusicObject(req, resp, paths, musicId);
			}
		}
	}

	private void sendMusicObject(HttpServletRequest req, HttpServletResponse resp, String[] paths, int musicId) throws IOException {
		Music music = MusicManager.instance().getMusic(musicId);
		if(music == null){
			resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			sendErrorMessage(resp,HttpServletResponse.SC_BAD_REQUEST, "Invalid id " + musicId);
		}
		else if(paths.length == 1){
			resp.getWriter().print(objectMapper.writeValueAsString(music));
		}
		else if(paths.length == 2){
			if(paths[1].equals(GET_FILE_SUBPATH)){
				sendFile(req, resp, music.getFilePath().toString());
			}
		}
	}

	private void sendFile(HttpServletRequest req, HttpServletResponse response, String filePath) throws IOException {
		System.out.println("Send file: " + filePath);
		
		File file = new File(filePath);
        ServletOutputStream outStream = response.getOutputStream();
        ServletContext context  = getServletConfig().getServletContext();
        String mimetype = context.getMimeType(filePath);
       
        // sets response content type
        if (mimetype == null) {
        	mimetype = Files.probeContentType(file.toPath());
        	if(mimetype == null){
        		mimetype = "application/octet-stream";
        	}
        }
        response.setContentType(mimetype);
        response.setContentLength((int)file.length());

        System.out.println("Mime type: " + mimetype + "; size: " + file.length());
        
        // sets HTTP header
        response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");
       
        byte[] byteBuffer = new byte[BUFSIZE];
        DataInputStream in = null;
        try{
	        in = new DataInputStream(new FileInputStream(file));
	        
	        System.out.println("Start sending file");
	        // reads the file's bytes and writes them to the response stream
	        int length   = 0;
	        while ((in != null) && ((length = in.read(byteBuffer)) != -1))
	        {
	            outStream.write(byteBuffer,0,length);
	        }
        }
        finally{
        	if(in != null){
        		in.close();
        	}
            outStream.close();
        }
	}

	private void sendErrorMessage(HttpServletResponse resp, int errCode, String errMessage) throws IOException {
		try {
			Error error = new Error(errCode, errMessage);
			String jsonMessage = objectMapper.writeValueAsString(error);
			resp.getWriter().print(jsonMessage);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} 
	}

	private void sendMusicList(HttpServletResponse resp) throws JsonProcessingException, IOException {
		List<Music> musics = MusicManager.instance().getMusics();		
		String json = objectMapper.writeValueAsString(musics);
		
		resp.getWriter().println(json);
	}
}
