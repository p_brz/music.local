package org.example.musiclocal;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.apache.commons.io.filefilter.IOFileFilter;

public class MimeFileFile implements IOFileFilter {

	private String mimeType;
	private String mimeSubtype;

	public MimeFileFile(String mimeType) {
		this(mimeType, null);
	}
	public MimeFileFile(String mimeType, String mimeSubtype) {
		this.mimeType = mimeType;
		this.mimeSubtype = mimeSubtype;
	}

	@Override
	public boolean accept(File file) {
		try {
			String contentType = Files.probeContentType(file.toPath());
			if(contentType != null){
				int sepIndex = contentType.indexOf("/");
				String fileType = contentType.substring(0, sepIndex);
				String fileSubtype = contentType.substring(sepIndex + 1, contentType.length());
				
				return fileType.equals(mimeType) && (this.mimeSubtype == null || fileSubtype.equals(mimeSubtype));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean accept(File dir, String filename) {
		//Ignora extensão do arquivo
		return true;
	}

}
