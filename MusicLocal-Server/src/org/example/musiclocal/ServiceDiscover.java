package org.example.musiclocal;
import java.io.IOException;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceInfo;


public class ServiceDiscover {
	private static ServiceDiscover singleton;

	public static ServiceDiscover instance() {
		if (singleton == null) {
			//Try get the lock (if singleton == null)
			synchronized (ServiceDiscover.class) {
				//Is still null after acquire the lock?
				if(singleton == null){
					singleton = new ServiceDiscover();
				}
			}
		}
		return singleton;
	}
	
	private JmDNS jmdns;
	private final Map<String, ServiceInfo> publishedServices;
	private final Thread shutdownHook;
	
	public ServiceDiscover() {
		publishedServices = new HashMap<String, ServiceInfo>();
		shutdownHook = createShutdownHook();
	}

	public synchronized void start() throws IOException {
		if(!hasStarted()){
	        System.out.println("Opening JmDNS...");
	        InetAddress ip = IpUtils.getIPv4Address();
	        this.jmdns = JmDNS.create(ip);

	    	Runtime.getRuntime().addShutdownHook(shutdownHook);
		}
	}

	public synchronized boolean hasStarted() {
		return this.jmdns != null;
	}
	public synchronized void stop() throws IOException {
		if(hasStarted()){
			System.out.println("Closing JmDNS...");
	        jmdns.unregisterAllServices();
	        jmdns.close();
	        jmdns = null;
	        
	        Runtime.getRuntime().removeShutdownHook(shutdownHook);
		}
	}
	
	public synchronized boolean hasPublished(String serviceName){
		return this.publishedServices.containsKey(serviceName);
	}
	public synchronized ServiceInfo publishService(String serviceType, String serviceName, int port) throws IOException {
		return this.publishService(serviceType, serviceName, port, "");
	}
	public synchronized ServiceInfo publishService(String serviceType, String serviceName, int port,
			String text) throws IOException {
		if(hasPublished(serviceName)){
			throw new IllegalArgumentException("A service with name \"" + serviceName + "\" "
					+ "already has been published.");
		}
		
        ServiceInfo service = ServiceInfo.create(serviceType, serviceName, port, text);
        try {
			jmdns.registerService(service);
	        return service;
		} catch (IOException e) {
			e.printStackTrace();
			if(this.publishedServices.containsKey(serviceName)){
				this.publishedServices.remove(serviceName);
			}
			throw e;
		}
	}
	
	private final Thread createShutdownHook() {
		return new Thread(){
    		@Override
    		public void run() {
    			System.out.println("Shutdown hook");
    			try {
					ServiceDiscover.instance().stop();
				} catch (IOException e) {
					e.printStackTrace();
				}
    		}
    	};
	}
}
