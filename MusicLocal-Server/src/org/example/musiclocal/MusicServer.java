package org.example.musiclocal;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.Servlet;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.NetworkConnector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class MusicServer{
	private static final String API_MUSICPATH = "/music/*";
	
	private final Server server;
	private ServletContextHandler commandsContextHandler;
	
	public MusicServer(int port) {
		server = new Server(port);
	}
	
	public List<Integer> getPorts(){
		List<Integer> ports = new ArrayList<Integer>();
		
		Connector[] connectors = this.server.getConnectors();
		for(Connector conn : connectors){
			if(conn instanceof NetworkConnector){
				@SuppressWarnings("resource")
				NetworkConnector netConn = (NetworkConnector)conn;
				int port = netConn.getLocalPort();
				if(port >= 0){
					ports.add(port);
				}
			}
		}
		
		return ports;
	}
	
	public void start() {
        commandsContextHandler = new ServletContextHandler(
                ServletContextHandler.SESSIONS);
        commandsContextHandler.setContextPath("/");
        
        Servlet musicServlet = new MusicServlet();
        ServletHolder servletHolder = new ServletHolder(musicServlet);
        commandsContextHandler.addServlet(servletHolder, API_MUSICPATH);
        
        CommandListServlet commandsServlet = new CommandListServlet();
        commandsServlet.addCommand(new Command("musics_url", API_MUSICPATH));
        
        ServletHolder commandsServletHolder = new ServletHolder(commandsServlet);
        commandsContextHandler.addServlet(commandsServletHolder, "/");
        
        
        HandlerList handlerList = new HandlerList();
        handlerList.setHandlers(new Handler[]{
        		  commandsContextHandler
        		, new DefaultHandler()});
        server.setHandler(handlerList);
        
		try {
			server.start();
		} catch (Exception e) {
			throw new IllegalStateException("Could not start server!",e);
		}
	}
	public void stop() {
		try {
			if(server.isRunning()){
				server.stop();
				server.destroy();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
