package org.example.musiclocal.database;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.example.musiclocal.Music;

public class MusicDAO {
	private static final String TABLE_NAME =  "music";
	private static final String COLUMN_ID = "id";
	private static final String COLUMN_LASTMODIFIED = "last_modified_timestamp";
	private static final String COLUMN_FILEPATH = "filepath";

	private static final String STATEMENT_SELECT_ALL = "SELECT * FROM " + TABLE_NAME;
	private static final String STATEMENT_SELECT_SUBFILES 
				= "SELECT * FROM " + TABLE_NAME + " WHERE " + COLUMN_FILEPATH + " LIKE ?";
	
	private static final String STATEMENT_SELECT_ITEM = STATEMENT_SELECT_ALL + " WHERE " + COLUMN_ID + " = ?";
	private static final String STATEMENT_UPDATE = "UPDATE " + TABLE_NAME + " SET "
			+ COLUMN_FILEPATH + " = ? " 
			+ "," + COLUMN_LASTMODIFIED + " = ? " 
			+ "WHERE " + COLUMN_FILEPATH + " = ?";
	private static final String STATEMENT_INSERT = "INSERT INTO " + TABLE_NAME 
			+ "(" + COLUMN_FILEPATH + "," + COLUMN_LASTMODIFIED + ")" + "VALUES(?, ?);";
	private static final String STATEMENT_DELETE_ALL_PREFIX = 
			"DELETE FROM " + TABLE_NAME + " WHERE " + COLUMN_ID + " IN ";
	
	private Database database;
	public MusicDAO(Database database) {
		this.database = database;
	}

	public void insertOrUpdate(Collection<Music> musics) {
		try(Connection conn = database.connect()) {
			conn.setAutoCommit(false);
			//PreparedStatement insertOrUpdateStatement = conn.prepareStatement(STATEMENT_INSERT_OR_REPLACE);
			PreparedStatement updateStatement = conn.prepareStatement(STATEMENT_UPDATE);
			PreparedStatement insertStatement = conn.prepareStatement(STATEMENT_INSERT);
			for(Music music : musics){
//				prepareInsertOrUpdateStament(insertOrUpdateStatement, music);
//				insertOrUpdateStatement.executeUpdate();
				prepareUpdateStatement(updateStatement, music);
				if(updateStatement.executeUpdate() ==0){//Se não atualizou, insere
					prepareInsertStament(insertStatement, music);
					insertStatement.executeUpdate();
				}
			}
			conn.commit();
		} catch (SQLException | IOException e) {
			e.printStackTrace();			
			throw new RuntimeException(e);
		}
	}

	public Music get(int musicId){
		
		try (Connection conn = database.connect()){
			PreparedStatement statement = createGetStatement(conn, musicId);
			ResultSet resultSet = statement.executeQuery();
			while(resultSet.next()){
				return parse(resultSet);
			}
			
		} catch (SQLException | IOException e) {
			e.printStackTrace();			
			throw new RuntimeException(e);
		}
		
		return null;
	}

	public List<Music> getAll() throws SQLException, IOException {
		return this.getAll(null);
	}
	public List<Music> getAll(File musicDir) throws SQLException, IOException  {
		List<Music> musics = new ArrayList<Music>();
		
		try (Connection conn = database.connect()){
			PreparedStatement statement = createGetAllStatement(conn, musicDir);
			ResultSet resultSet = statement.executeQuery();
			while(resultSet.next()){
				musics.add(parse(resultSet));
			}
			
		} catch (SQLException | IOException e) {
			e.printStackTrace();			
			throw e;
		}
		
		return musics;
	}
	public void removeAll(List<Music> musics) {
		if(musics.isEmpty()){
			return;
		}
		try(Connection conn = database.connect()) {
			PreparedStatement deleteAllStatement = createDeleteAllStatement(conn, musics);
			int deleted = deleteAllStatement.executeUpdate();
			System.out.println("Deleted " + deleted + " rows");
		} catch (SQLException | IOException e) {
			e.printStackTrace();			
			throw new RuntimeException(e);
		}
	}

	private Music parse(ResultSet resultSet) throws SQLException {
		String filepath = resultSet.getString(COLUMN_FILEPATH);
		Music music = new Music(resultSet.getLong(COLUMN_ID), new File(filepath));
		music.setLastModifiedTimestamp(resultSet.getLong(COLUMN_LASTMODIFIED));
		return music;
	}

	private PreparedStatement createGetStatement(Connection conn, long musicId) throws SQLException {
		PreparedStatement preparedStatement = conn.prepareStatement(STATEMENT_SELECT_ITEM);
		preparedStatement.setLong(1, musicId);
		return preparedStatement;
	}

	private PreparedStatement createGetAllStatement(Connection conn, File rootDir) throws SQLException {
		if(rootDir == null){
			return conn.prepareStatement(STATEMENT_SELECT_ALL);
		}
		else{
			PreparedStatement statement = conn.prepareStatement(STATEMENT_SELECT_SUBFILES);
			statement.setString(1, getFilePathString(rootDir) + "%");
			return statement;
		}
	}
	
	private String getFilePathString(File file) {
		Path filePath = file.toPath();
		try {
			return filePath == null? "" : filePath.toFile().getCanonicalPath();
		} catch (IOException e) {
			return filePath.toAbsolutePath().toString();
		}
	}

	private void prepareInsertStament(PreparedStatement insertStatement, Music music)  throws SQLException, IOException {
		prepareInsertOrUpdateStament(insertStatement, music);
	}

	private void prepareUpdateStatement(PreparedStatement updateStatement, Music music) throws SQLException, IOException {
		prepareInsertOrUpdateStament(updateStatement, music);
		updateStatement.setString(3, music.getFilePathString());
	}
	private void prepareInsertOrUpdateStament( PreparedStatement insertOrUpdateStatement, Music music) throws SQLException, IOException {
		insertOrUpdateStatement.setString(1, music.getFilePathString());
		insertOrUpdateStatement.setLong(2, music.getLastModifiedTimestamp());
	}
	
	private PreparedStatement createDeleteAllStatement(Connection conn, List<Music> musics) throws SQLException {
		
		StringBuilder builder = new StringBuilder();
		builder.append(STATEMENT_DELETE_ALL_PREFIX).append("(");
		
		boolean first = true;
		for(Music music : musics){
			if(!first){
				builder.append(", ");
			}
			else{
				first = false;
			}
			builder.append(music.getId());
		}
		builder.append(")");
		
		String deleteStatement = builder.toString();
		
		System.out.println(deleteStatement);
		
		return conn.prepareStatement(deleteStatement);
	}
}
