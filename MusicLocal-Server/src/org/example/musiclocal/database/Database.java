package org.example.musiclocal.database;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Database {
	static private Database singleton;
	static public Database instance(){
		if(singleton == null){
			singleton = new Database();
		}
		return singleton;
	}
	
	private static final String CONNECTION_PREFIX = "jdbc:sqlite:";
	
	public Connection connect() throws SQLException, IOException{
		boolean dbCreated = this.exists();
		Connection connection = DriverManager.getConnection(getConnectionUrl());
		
		if(!dbCreated){
			try{
				connection.setAutoCommit(false);
				create(connection);
				connection.commit();
				connection.setAutoCommit(true);
			}
			catch(SQLException ex){
				connection.rollback();
				try {
					deleteDatabase();
				} catch (IOException e) {
					e.printStackTrace();
					e.initCause(ex);
					throw new IOException("Could not remove invalid database",e);
				}
				
				throw new SQLException("Could not created database", ex);
			}
		}
		
		return connection;
	}

	private void deleteDatabase() throws IOException {
		Files.delete(Paths.get(this.getDatabaseFilepath()));
	}

	protected void create(Connection connection) throws SQLException {
		Statement statement = connection.createStatement();
		createMusicsTable(statement);
	}

	private void createMusicsTable(Statement statement) throws SQLException {
	      statement.executeUpdate("CREATE TABLE music("
	      		+ "id INTEGER PRIMARY KEY"
	      		+ ", filepath    TEXT UNIQUE"
	      		+ ", last_modified_timestamp INTEGER"
	      		+ ");");
	}

	private boolean exists() {
		return Files.exists(Paths.get(getDatabaseFilepath()));
	}
	private String getDatabaseFilepath() {
		return "music.local.db";
	}

	protected String getConnectionUrl() {
		return CONNECTION_PREFIX + getDatabaseFilepath();
	}
}
