package org.example.musiclocal.database;


public class MusicDatabase {
	private static MusicDatabase singleton;
	public static MusicDatabase instance() {
		if (singleton == null) {
			singleton = new MusicDatabase();
		}
		return singleton;
	}
	private Database database;
	private MusicDAO musicDAO;
	
	public MusicDatabase() {
		database = new Database();
		musicDAO = new MusicDAO(database);
	}
	public MusicDAO musics() {
		return this.musicDAO;
	}

}
