package org.example.musiclocal;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class OptionsManager {
	private static final String APPLICATION_NAME = "music.local";
	
	public enum Option{
		MusicDir   ("d", "musicdir", true, "Define a pasta raiz de músicas que será utilizada." 
										+ "Adiciona recursivamente todas as músicas encontradas a partir deste diretório."
										+ "Se não definido, utiliza todas as músicas conhecidas.")
	  , ServiceName("n", "name", true,  "Define o nome que o servidor utilizará para se anunciar aos clientes."
  							+ " Se não for definida, um nome padrão é utilizado.")
	  , ServerPort ("p", "port", true,  "Define a porta em que o servidor será executado."
										+ " Se não for definido, será utilizada uma porta dinâmica (definida pelo sistema operacional).")
	  , Help       ("h", "help", false,  "Imprime a mensagem de ajuda e finaliza.");
		
		public final String shortName;
		public final String longName;
		public final String description;
		public final boolean hasArg;
		private Option(String shortName, String longName, boolean hasArg, String description){
			this.shortName = shortName;
			this.longName = longName;
			this.description = description;
			this.hasArg = hasArg;
		}
	}

	private final Options options;
	private final CommandLineParser commandLineParser;
	private CommandLine commands;
	private String applicationName;
	
	public OptionsManager() {
		options = new Options();
		commandLineParser = new DefaultParser();
		applicationName = APPLICATION_NAME;
		addOptions(options);
	}
	
	private void addOptions(Options options) {
		for(Option opt : Option.values()){
			options.addOption(opt.shortName, opt.longName, opt.hasArg, opt.description);
		}
	}

	public void setup(String[] args) throws ParseException {
		commands = commandLineParser.parse(this.options, args);
	}
	public void printHelpMessage(){
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp( this.applicationName, options ,true);
	}

	public String getOptionValue(Option opt) {
		return this.getOptionValue(opt, null);
	}
	public String getOptionValue(Option opt, String defaultValue) {
		if(commands == null){
			throw new IllegalStateException("Should call setup before trying to get Option value.");
		}
		if(opt.shortName != null && commands.hasOption(opt.shortName)){
			return commands.getOptionValue(opt.shortName, defaultValue);
		}
		else if(opt.longName != null && commands.hasOption(opt.longName)){
			return commands.getOptionValue(opt.longName, defaultValue);
		}
		return defaultValue;
	}

	public boolean containsOption(Option option) {
		if(this.commands != null){
			return commands.hasOption(option.shortName)
					|| commands.hasOption(option.longName);
		}
		return false;
	}

}
