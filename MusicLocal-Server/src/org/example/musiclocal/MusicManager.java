package org.example.musiclocal;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.HiddenFileFilter;
import org.example.musiclocal.database.MusicDatabase;


public class MusicManager {
	private static MusicManager singleton;
	public static MusicManager instance() {
		if(singleton == null){
			singleton = new MusicManager();
			singleton.removeInexistentFiles();
		}
		return singleton;
	}

	private File musicDir;
	public MusicManager() {
		musicDir = null;
	}
	
	public Music getMusic(int musicId) {
		return MusicDatabase.instance().musics().get(musicId);
	}

	public List<Music> getMusics() {
		try {
			return MusicDatabase.instance().musics().getAll(musicDir);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	public void setMusicRoot(File fileDir) {
		if(!fileDir.exists() || !fileDir.isDirectory()){
			throw new IllegalArgumentException("Argument musicDir should be a directory!");
		}
		this.musicDir = fileDir;
		listMusicFiles(fileDir, fileDir.lastModified());
	}

	//FIXME: (?)handle deleted files
	private void listMusicFiles(File musicDir, Long lastModifiedTimestamp) {
		List<Music> newMusics = new ArrayList<Music>();
		Iterator<File> filesIterator = FileUtils.iterateFiles(musicDir, new MimeFileFile("audio"), HiddenFileFilter.VISIBLE);
		while(filesIterator.hasNext()){
			File file = filesIterator.next();			
			newMusics.add(new Music(file));
		}

		MusicDatabase.instance().musics().insertOrUpdate(newMusics);
	}

	private void removeInexistentFiles() {
		List<Music> musicsToDelete = new LinkedList<Music>();
		List<Music> musics = getMusics();
		for(Music music : musics){
			if(!Files.exists(music.getFilePath())){
				musicsToDelete.add(music);
			}
		}
		MusicDatabase.instance().musics().removeAll(musicsToDelete);
	}
}
