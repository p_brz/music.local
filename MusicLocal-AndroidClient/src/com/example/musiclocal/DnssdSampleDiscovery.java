package com.example.musiclocal;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceEvent;
import javax.jmdns.ServiceInfo;
import javax.jmdns.ServiceListener;
import javax.jmdns.ServiceTypeListener;

import org.apache.http.conn.util.InetAddressUtils;

import android.app.Activity;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.MulticastLock;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;

public class DnssdSampleDiscovery extends Activity {

    private static final String SERVICE_TYPE = "_http._tcp.local.";
	private static final String PACKAGE_NAME = DnssdSampleDiscovery.class.getPackage().getName() ;
    private static final String MULTICAST_LOCK_TAG = PACKAGE_NAME + ".MULTICAST_LOCK" ;
    
	MulticastLock lock;
    Handler handler = new android.os.Handler();
    JmDNS jmdns;
    ServiceListener serviceListener;
	private ServiceTypeListener serviceTypeListener;
    
    class DebugServiceListener implements ServiceListener{

		@Override
		public void serviceAdded(ServiceEvent servEvt) {
			print("Added service: " + servEvt.getName());
		}

		@Override
		public void serviceRemoved(ServiceEvent servEvt) {
			print("Removed service: " + servEvt.getName());
		}

		@Override
		public void serviceResolved(ServiceEvent servEvt) {
			print("Resolved service: " + servEvt.getName());
		}
    }
    
    class DebugServiceTypeListener implements ServiceTypeListener{

		@Override
		public void serviceTypeAdded(ServiceEvent arg0) {
			print("Add service type: " + arg0.getType());
		}

		@Override
		public void subTypeForServiceTypeAdded(ServiceEvent arg0) {
			print("Addedd subtype: " + arg0.getType());
		}
    	
    }
    
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        serviceListener = new DebugServiceListener();
        serviceTypeListener = new DebugServiceTypeListener();
    }  
    
    @Override
    protected void onResume() {
    	super.onResume();
    	
    	startServiceBrowser();
    }

	@Override
    protected void onPause() {
    	super.onPause();
    	
    	stopServiceBrowser();
    }
	private void startServiceBrowser() {
		this.lock = acquireLock();
		browseOnBackground();
	}

	private void stopServiceBrowser() {
    	releaseLock();
    	stopBackgroundBrowsing();
	}

	private MulticastLock acquireLock() {
		WifiManager wifiManager = (WifiManager)getSystemService(WIFI_SERVICE);
		MulticastLock lock =wifiManager.createMulticastLock(MULTICAST_LOCK_TAG);
		
		lock.setReferenceCounted(true);
		lock.acquire();
		
		return lock;
	}
    private void releaseLock() {
    	if(lock != null){
    		lock.release();
    		lock = null;
    	}
	}
    
	private void browseOnBackground() {
		new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... arg0) {
				try {
					print("prepare to create");
					
					Collection<InetAddress> ips = getIPAddresses();
					InetAddress hostAddress = null;
					
					for(InetAddress addr: ips){
						if(addr instanceof Inet4Address){
							hostAddress = addr;
							break;
						}
					}
					
					Log.d(getClass().getName(), "Host address: " + hostAddress);
					
					if(hostAddress != null){
						jmdns = JmDNS.create(hostAddress);
					}
					else{
						jmdns = JmDNS.create();
					}
					print("created at hostname: " + jmdns.getHostName());
					print("in interface: " + jmdns.getInterface());
					
					jmdns.addServiceListener(SERVICE_TYPE, serviceListener);
					jmdns.addServiceTypeListener(serviceTypeListener);
					
					ServiceInfo services[] = jmdns.list(SERVICE_TYPE);
					
					for(ServiceInfo info: services){
						print("Found: " + info.getName());
					}
					
					if(services.length == 0){
						print("Not found any service");
					}
				} catch (IOException e) {
					e.printStackTrace();
					print("oops");
				}
				return null;
			}
	    }.execute();
	}

	private void stopBackgroundBrowsing() {
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... arg0) {
				try {
					System.out.println("Stopping serviceFinder");
					jmdns.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;
			}
	    }.execute();
	}


	private void print(final String text) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				TextView txt = (TextView)findViewById(R.id.mainTxt);
				txt.setText(txt.getText().toString() + "\n" + text);
			}
		});
	}
	
	/**
     * Get IP address from first non-localhost interface
     * @return  address or null
     */
    public static String getIPAddressStr() {
        return getIPAddressStr(false);
    }
	/**
     * Get IP address from first non-localhost interface
     * @param ipv4  true=return ipv4, false=return ipv6
     * @return  address or null
     * 
     * Adaptado de: http://stackoverflow.com/a/13007325
     */
    public static String getIPAddressStr(boolean useIPv4) {

        List<NetworkInterface> interfaces;
		try {
			interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			return null;
		}
        for (NetworkInterface intf : interfaces) {
            List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
            for (InetAddress addr : addrs) {
                if (!addr.isLoopbackAddress()) {
                    String sAddr = addr.getHostAddress().toUpperCase(Locale.US);
                    boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr); 
                    if (useIPv4) {
                        if (isIPv4) 
                            return sAddr;
                    } else {
                        if (!isIPv4) {
                            int delim = sAddr.indexOf('%'); // drop ip6 port suffix
                            return delim<0 ? sAddr : sAddr.substring(0, delim);
                        }
                    }
                }
            }
        }
        return null;
    }

	/**
     * Get all IP address of non-localhost interface
     * @return  list of addresses
     * 
     * Adaptado de: http://stackoverflow.com/a/13007325
     */
    public static Collection<InetAddress> getIPAddresses() {

        List<NetworkInterface> interfaces;
		try {
			interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
        List<InetAddress> addrs = new ArrayList<InetAddress>();
        for (NetworkInterface intf : interfaces) {
        	Enumeration<InetAddress> ips = intf.getInetAddresses();
            while (ips.hasMoreElements()) {
                InetAddress addr = ips.nextElement();
            	if (!addr.isLoopbackAddress()) {
                    addrs.add(addr);
                }
            }
        }
        return addrs;
    }
}