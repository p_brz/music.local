package com.example.musiclocal;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.widget.RemoteViews;

public class ForegroundServiceCreator {
	private static ForegroundServiceCreator singleton;
	public static ForegroundServiceCreator instance(){
		if(singleton == null){
			singleton = new ForegroundServiceCreator();
		}
		return singleton;
	}
	
	private MusicPlayerService musicService;
	
	public Notification createForegroundNotification(MusicPlayerService musicPlayerService) {
		this.musicService = musicPlayerService;
		Context context = (Context)musicPlayerService;
		NotificationCompat.Builder notifBuilder = new NotificationCompat.Builder(context);
		
		/*smallIcon é necessário para notificação aparecer.*/
		notifBuilder.setSmallIcon(R.drawable.ic_action_music_1);
		notifBuilder.setContent(createServiceNotificationView(context));
		notifBuilder.setContentIntent(createServiceSettingsPendingIntent(context));
		
		return notifBuilder.build();
	}
	
	private PendingIntent createServiceSettingsPendingIntent(Context context) {
		Class<ListMusicsActivity> activityClass = ListMusicsActivity.class;
		// Creates an explicit intent for an Activity in your app
		Intent contentIntent = new Intent(context, activityClass);
		// The stack builder object will contain an artificial back stack for the
		// started Activity.
		// This ensures that navigating backward from the Activity leads out of
		// your application to the Home screen.
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
		// Adds the back stack for the Intent (but not the Intent itself)
		stackBuilder.addParentStack(activityClass);
		// Adds the Intent that starts the Activity to the top of the stack
		stackBuilder.addNextIntent(contentIntent);
		PendingIntent contentPendingIntent = stackBuilder.getPendingIntent(
		            0,
		            PendingIntent.FLAG_UPDATE_CURRENT
		        );
		return contentPendingIntent;
	}
	private RemoteViews createServiceNotificationView(Context context) {
		
		RemoteViews contentView = new RemoteViews(context.getPackageName(), R.layout.notification_foregroundservice);
		contentView.setImageViewResource(R.id.notification_image, R.drawable.ic_action_music_1);
		if(this.musicService != null && this.musicService.getCurrentMusic() != null){
			createIcons(contentView);
			createIntents(contentView);
		}
		contentView.setImageViewResource(R.id.close_icon, R.drawable.ic_close);
		
		PendingIntent pendingCloseIntent = createCloseServicePendingIntent(context);
		contentView.setOnClickPendingIntent(R.id.close_icon, pendingCloseIntent);
		
		return contentView;
	}

	private void createIcons(RemoteViews contentView) {
		contentView.setTextViewText(R.id.notification_title, musicService.getCurrentMusic().getFilename());
		int pauseResumeDrawable = musicService.isPlaying() ? R.drawable.ic_action_pause : R.drawable.ic_action_play;
		contentView.setImageViewResource(R.id.pauseResume_icon, pauseResumeDrawable);
		contentView.setImageViewResource(R.id.previous_icon, R.drawable.ic_action_previous);
		contentView.setImageViewResource(R.id.next_icon, R.drawable.ic_action_next);
	}
	
	private void createIntents(RemoteViews contentView) {
		createOnClickServiceIntent(contentView, R.id.pauseResume_icon, MusicPlayerService.ACTION_TOGGLEPLAY);
		createOnClickServiceIntent(contentView, R.id.previous_icon   , MusicPlayerService.ACTION_PREVIOUS);
		createOnClickServiceIntent(contentView, R.id.next_icon       , MusicPlayerService.ACTION_NEXT);
//		contentView.setOnClickPendingIntent(R.id.pauseResume_icon, createServiceIntent(MusicPlayerService.ACTION_TOGGLEPLAY));
//		contentView.setOnClickPendingIntent(R.id.previous_icon   , createServiceIntent(MusicPlayerService.ACTION_PREVIOUS));
//		contentView.setOnClickPendingIntent(R.id.next_icon 		 , createServiceIntent(MusicPlayerService.ACTION_NEXT));
	}

	private void createOnClickServiceIntent(RemoteViews remoteView, int view, String serviceAction) {
		remoteView.setOnClickPendingIntent(view, createServiceIntent(serviceAction));
	}
	private PendingIntent createServiceIntent(String action) {
		Intent pauseResumeIntent = new Intent(this.musicService,MusicPlayerService.class);
		pauseResumeIntent.setAction(action);
		return PendingIntent.getService(musicService, 0, pauseResumeIntent, 0);
	}


	private PendingIntent createCloseServicePendingIntent(Context context) {
		Intent closeServiceIntent = new Intent(context,MusicPlayerService.class);
		closeServiceIntent.setAction(MusicPlayerService.ACTION_FINISH);
		
		PendingIntent pendingCloseIntent = PendingIntent.getService(context, 0, closeServiceIntent, 0);
		return pendingCloseIntent;
	}
}
