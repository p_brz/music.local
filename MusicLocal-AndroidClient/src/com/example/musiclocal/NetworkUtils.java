package com.example.musiclocal;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;

import org.apache.http.conn.util.InetAddressUtils;

public class NetworkUtils {
	/**
     * Get IP address from first non-localhost interface
     * @return  address or null
     */
    public static String getIPAddressStr() {
        return getIPAddressStr(false);
    }
	/**
     * Get IP address from first non-localhost interface
     * @param ipv4  true=return ipv4, false=return ipv6
     * @return  address or null
     * 
     * Adaptado de: http://stackoverflow.com/a/13007325
     */
    public static String getIPAddressStr(boolean useIPv4) {

        List<NetworkInterface> interfaces;
		try {
			interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			return null;
		}
        for (NetworkInterface intf : interfaces) {
            List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
            for (InetAddress addr : addrs) {
                if (!addr.isLoopbackAddress()) {
                    String sAddr = addr.getHostAddress().toUpperCase(Locale.US);
                    boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr); 
                    if (useIPv4) {
                        if (isIPv4) 
                            return sAddr;
                    } else {
                        if (!isIPv4) {
                            int delim = sAddr.indexOf('%'); // drop ip6 port suffix
                            return delim<0 ? sAddr : sAddr.substring(0, delim);
                        }
                    }
                }
            }
        }
        return null;
    }

	/**
     * Get all IP address of non-localhost interface
     * @return  list of addresses
     * 
     * Adaptado de: http://stackoverflow.com/a/13007325
     */
    public static Collection<InetAddress> getIPAddresses() {

        List<NetworkInterface> interfaces;
		try {
			interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
        List<InetAddress> addrs = new ArrayList<InetAddress>();
        for (NetworkInterface intf : interfaces) {
        	Enumeration<InetAddress> ips = intf.getInetAddresses();
            while (ips.hasMoreElements()) {
                InetAddress addr = ips.nextElement();
            	if (!addr.isLoopbackAddress()) {
                    addrs.add(addr);
                }
            }
        }
        return addrs;
    }
}
