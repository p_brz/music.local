package com.example.musiclocal;

import android.content.Intent;
import android.os.Binder;
import android.util.Log;
import android.widget.MediaController.MediaPlayerControl;

/* ******************************************* Binder ********************************************** */
public class MusicPlayerBinder extends Binder implements MediaPlayerControl{
	private MusicPlayerService musicService;
	private MusicPlayer musicPlayer;
	public MusicPlayerBinder(MusicPlayerService context, MusicPlayer musicPlayer){
		this.musicService = context;
		this.musicPlayer = musicPlayer;
	}
	
	@Override
	public void start() {
		Log.d(getClass().getName(), "start!");
//		startService(MusicPlayerService.ACTION_RESUME);
		musicPlayer.resume();
	}
	@Override
	public void pause() {
		Log.d(getClass().getName(), "pause!");
//		startService(MusicPlayerService.ACTION_PAUSE);
		musicPlayer.pause();
	}

	public void togglePlay() {
		musicPlayer.togglePlay();
	}
	public void playNext() {
		Log.d(getClass().getName(), "play next!");
//		startService(MusicPlayerService.ACTION_NEXT);
		musicPlayer.playNext();
	}
	public void playPrevious() {
		Log.d(getClass().getName(), "play previous!");
//		startService(MusicPlayerService.ACTION_PREVIOUS);
		musicPlayer.playPrevious();
	}

//	private void startService(String action) {
//		Intent intent = new Intent(this.musicService, MusicPlayerService.class);
//		intent.setAction(action);
//		musicService.startService(intent);
//	}
	@Override
	public void seekTo(int pos) {
		musicPlayer.seekTo(pos);
	}
	@Override
	public int getBufferPercentage() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public int getDuration() {
		return musicPlayer.getDuration();
	}
	@Override
	public int getCurrentPosition() {
		return musicPlayer.getCurrentPosition();
	}
	@Override
	public boolean isPlaying() {
		return musicPlayer.isPlaying();
	}
	public boolean isPaused() {
		return musicPlayer.isPaused();
	}
//	@Override
//	public boolean canPause() {
//		boolean canPause = musicPlayer.canPause();
//
//		Log.d(getClass().getName(), "Can pause? " +  canPause);
//		
//		return canPause;
//	}
	@Override
	public boolean canPause() {
		return true;
	}
	@Override
	public boolean canSeekBackward() {
//		return musicPlayer.canSeekBackward();
		return true;
	}
	@Override
	public boolean canSeekForward() {
//		return musicPlayer.canSeekForward();
		return true;
	}
	@Override
	public int getAudioSessionId() {
		return musicPlayer.getAudioSessionId();
	}

	public boolean hasNext() {
		return musicPlayer.hasNext();
	}

	public boolean hasPrevious() {
		return musicPlayer.hasPrevious();
	}

	public void hideForegroundService() {
		musicService.hideForegroundService();
	}
	public void showForegroundService() {
		musicService.showForegroundService();
	}

	public void finish() {
		musicService.finishService();
	}

}