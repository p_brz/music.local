package com.example.musiclocal;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.util.Log;

import com.example.musiclocal.MusicLocalClient.MusicLoadResponse;

/* ******************************************* MusicPlayer ******************************************** */
public class MusicPlayer implements OnErrorListener, OnPreparedListener, OnCompletionListener{
	public interface PlayerListener {
		void onStart (Music music);
		void onResume(Music music);
		void onPause (Music music);
		void onStop  (Music music);
	}
	private MediaPlayer mediaPlayer;
	private boolean started;
	private final List<PlayerListener> playerListeners;
	private final List<Music> playlist;
	private int currentMusicIndex;
//	private Music currentMusic;
	private MusicLocalClient.OnMusicLoadedListener delegateMusicLoadedListener;
	
	public MusicPlayer() {
		playerListeners = new ArrayList<MusicPlayer.PlayerListener>();
		playlist = new ArrayList<Music>();
		started = false;
		currentMusicIndex = 0;
		
		mediaPlayer = new MediaPlayer();
		mediaPlayer.setOnErrorListener(this);
		mediaPlayer.setOnPreparedListener(this);
		mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		mediaPlayer.setOnCompletionListener(this);
	}

	/* *********************************************************************************************/
	public MusicLocalClient.OnMusicLoadedListener getMusicLoadedListener() {
		return delegateMusicLoadedListener;
	}
	public void setMusicLoadedListener(MusicLocalClient.OnMusicLoadedListener musicLoadedListener) {
		this.delegateMusicLoadedListener = musicLoadedListener;
	}
	/* **************************************** Query methods **************************************/
	public synchronized boolean isPlaying() {
		return mediaPlayer.isPlaying();
	}
	public synchronized boolean isPaused() {
		return !mediaPlayer.isPlaying() && isPrepared();
	}
	public synchronized boolean canPause() {
		return isPrepared() && isPlaying();
	}
	private boolean isPrepared() {
		return started;
	}
	public synchronized int getCurrentPosition() {
		if(isPrepared()){
			return mediaPlayer.getCurrentPosition();
		}
		return 0;
	}

	public synchronized int getDuration() {
		if(isPrepared()){
			return mediaPlayer.getDuration();
		}	
		return 0;
	}
	public boolean canSeekBackward() {
		return isPlaying() && getCurrentPosition() > 0;
	}
	public boolean canSeekForward() {
		return isPlaying();
	}
	public int getAudioSessionId() {
		return mediaPlayer.getAudioSessionId();
	}
	/* *********************************************************************************************/
	public void togglePlay() {
		if(isPlaying()){
			pause();
		}
		else if(isPaused()){
			resume();
		}
	}
	public synchronized void resume() {
		this.mediaPlayer.start();
		musicResumed();
	}
	public synchronized void pause() {
		this.mediaPlayer.pause();
		musicPaused();
	}
	public synchronized void seekTo(int pos) {
		if(isPrepared()){
			mediaPlayer.seekTo(pos);
		}	
	}
	public synchronized void stop() {
		this.mediaPlayer.stop();
	}
	public synchronized void playPrevious() {
		if(hasPrevious()){
			play(getPrevious());
		}
	}
	public synchronized void playNext() {
		if(hasNext()){
			play(getNext());
		}
	}
	public synchronized boolean hasNext() {
		return currentMusicIndex + 1 < this.playlist.size();
	}
	public synchronized boolean hasPrevious() {
		return currentMusicIndex - 1 >= 0 && playlist.size() > 0;
	}
	public synchronized Music getCurrentMusic() {
		//return this.currentMusic;
		return currentMusicIndex >= this.playlist.size() ? null : playlist.get(currentMusicIndex);
	}
	private Music getCurrentMusicClone() {
		Music currentMusic = getCurrentMusic();
		if(currentMusic != null){
			return new Music(currentMusic);
		}
		return null;
	}
	public synchronized void play(final Music music){
		MusicLocalClient.instance().loadMusic(music, new MusicLocalClient.OnMusicLoadedListener() {
			@Override
			public void onMusicLoad(MusicLoadResponse response) {
				playMusicFile(music, response.getFile());
				if(delegateMusicLoadedListener != null){
					delegateMusicLoadedListener.onMusicLoad(response);
				}
			}
			@Override
			public void onMusicLoadFailure(Music music, Throwable cause) {
				if(delegateMusicLoadedListener != null){
					delegateMusicLoadedListener.onMusicLoadFailure(music, cause);;
				}
			}
			@Override
			public void onStartLoad(Music music) {
				if(delegateMusicLoadedListener != null){
					delegateMusicLoadedListener.onStartLoad(music);
				}
			}
			@Override
			public void onProgressLoading(Music music, float progress) {
				if(delegateMusicLoadedListener != null){
					delegateMusicLoadedListener.onProgressLoading(music, progress);
				}
			}
		});
	}

	public synchronized void play(Collection<Music> playlist) {
		this.play(playlist, 0);
	}
	public synchronized void play(Collection<Music> playlist, int startIndex) {
		this.playlist.clear();
		this.currentMusicIndex = startIndex;
		if(playlist.size() > 0){
			this.playlist.addAll(playlist);
			play(getCurrentMusic());
		}
	}
	
	/* ************************************* Auxiliary methods ************************************************/
	private void playMusicFile(final Music music, File musicFile) {
		FileInputStream inputStream;
		try {				
			inputStream = new FileInputStream(musicFile);
			mediaPlayer.reset();
			
			musicStopped();
			setCurrentMusic(music);			
			
			mediaPlayer.setDataSource(inputStream.getFD());
			mediaPlayer.prepareAsync();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void setCurrentMusic(Music music) {
		int musicIndex = playlist.size() > 0 ? playlist.indexOf(music) : -1;
		if(musicIndex < 0){
			this.playlist.clear();
			this.playlist.add(music);
			this.currentMusicIndex = 0;
		}
		else{
			this.currentMusicIndex = musicIndex;
		}
	}
	private Music getNext() {
		return this.playlist.get(currentMusicIndex + 1);
	}
	private Music getPrevious() {
		return this.playlist.get(currentMusicIndex - 1);
	}
	/* ************************************* PlayerListeners ************************************************/
	public synchronized void addPlayerListener(PlayerListener listener) {
		this.playerListeners.add(listener);
	}
	public synchronized void removePlayerListener(PlayerListener listener){
		this.playerListeners.remove(listener);		
	}

	/* *****************************************************************************************************/
	@Override
	public void onPrepared(MediaPlayer mp) {
		mp.start();
		musicStarted(getCurrentMusic());
	}

	@Override
	public boolean onError(MediaPlayer mp, int what, int extra) {
		//FIXME: tratar erro!
		Log.e(getClass().getName(), "Ocorreu um erro com o MediaPlayer: " + mp);
		return true;
	}
	@Override
	public void onCompletion(MediaPlayer mp) {
		playNext();
	}
	/* *****************************************************************************************************/
	private void musicStarted(Music music) {
		this.started = true;
		for(PlayerListener listener : this.playerListeners){
			listener.onStart(music);
		}
	}
	private void musicStopped() {
		started = false;
		for(PlayerListener listener : this.playerListeners){
			listener.onStop(getCurrentMusicClone());
		}
	}
	private void musicResumed() {
		for(PlayerListener listener : this.playerListeners){
			listener.onResume(getCurrentMusicClone());
		}
	}
	private void musicPaused() {
		for(PlayerListener listener : this.playerListeners){
			listener.onPause(getCurrentMusicClone());
		}
	}
}