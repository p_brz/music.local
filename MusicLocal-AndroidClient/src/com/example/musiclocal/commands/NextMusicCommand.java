package com.example.musiclocal.commands;

import com.example.musiclocal.MusicPlayer;

import android.content.Intent;

public class NextMusicCommand implements CommandExecutor {
	MusicPlayer musicPlayer;
	public NextMusicCommand(MusicPlayer musicPlayer) {
		this.musicPlayer = musicPlayer;
	}

	@Override
	public void execute(Intent intent, int startId) {
		musicPlayer.playNext();
	}

}
