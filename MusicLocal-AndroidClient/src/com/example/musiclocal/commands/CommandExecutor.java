package com.example.musiclocal.commands;

import android.content.Intent;

/* **************************************************************************************************** */
public interface CommandExecutor{
	void execute(Intent intent, int startId);
}