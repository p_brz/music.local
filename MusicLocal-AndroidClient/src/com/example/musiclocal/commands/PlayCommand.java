package com.example.musiclocal.commands;

import java.util.Collection;

import android.content.Intent;

import com.example.musiclocal.Music;
import com.example.musiclocal.MusicPlayer;
import com.example.musiclocal.MusicPlayerService;

public class PlayCommand implements CommandExecutor{
	private MusicPlayer musicPlayer;	
	public PlayCommand(MusicPlayer service) {
		this.musicPlayer = service;
	}
	
	
	@Override
	public void execute(Intent intent, int startId) {
		if(!intentIsValid(intent)){
			//TODO: notificar erro!
			return;
		}
		
		if(intent.getExtras().containsKey(MusicPlayerService.EXTRA_PLAYLIST)){
			@SuppressWarnings("unchecked")
			Collection<Music> playlist = (Collection<Music>)intent.getSerializableExtra(MusicPlayerService.EXTRA_PLAYLIST);
			
			int startIndex = intent.getExtras().getInt(MusicPlayerService.EXTRA_PLAYLIST_STARTINDEX, 0);
			musicPlayer.play(playlist, startIndex);
		}
		else{
			final Music music = (Music)intent.getSerializableExtra(MusicPlayerService.EXTRA_MUSIC);
			musicPlayer.play(music);			
		}
	}


	private boolean intentIsValid(Intent intent) {
		return intent != null && intent.getExtras() != null && 
				(intent.getExtras().containsKey(MusicPlayerService.EXTRA_MUSIC)
						|| intent.getExtras().containsKey(MusicPlayerService.EXTRA_PLAYLIST));
	}
}