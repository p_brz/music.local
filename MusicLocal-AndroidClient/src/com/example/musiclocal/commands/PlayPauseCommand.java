package com.example.musiclocal.commands;

import com.example.musiclocal.MusicPlayer;
import com.example.musiclocal.MusicPlayerService;

import android.content.Intent;

/** Pause or Resume player*/
public class PlayPauseCommand implements CommandExecutor{
	MusicPlayer musicPlayer;
	
	public PlayPauseCommand(MusicPlayer service) {
		this.musicPlayer = service;
	}
	@Override
	public void execute(Intent intent, int startId) {
		if(isPauseCommand(intent)){
			musicPlayer.pause();
		}
		else if(isResumeCommand(intent)){
			musicPlayer.resume();
		}
	}
	private boolean isResumeCommand(Intent intent) {
		if(musicPlayer.isPaused()){ 
			return MusicPlayerService.ACTION_RESUME    .equals(intent.getAction()) 
			    || MusicPlayerService.ACTION_TOGGLEPLAY.equals(intent.getAction());
		}
		return false;
	}
	private boolean isPauseCommand(Intent intent) {
		if(musicPlayer.isPlaying()){ 
				return MusicPlayerService.ACTION_PAUSE     .equals(intent.getAction()) 
				    || MusicPlayerService.ACTION_TOGGLEPLAY.equals(intent.getAction());
		}
		return false;
	}
}