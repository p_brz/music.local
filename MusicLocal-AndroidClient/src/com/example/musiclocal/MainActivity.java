package com.example.musiclocal;

import java.io.IOException;
import java.util.ArrayList;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceEvent;
import javax.jmdns.ServiceInfo;
import javax.jmdns.ServiceListener;

import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {
	ArrayList<ServiceInfo> services;
	JmDNS serviceFinder;
	WifiManager.MulticastLock lock;
	TextView txtView;
	
	ServiceListener mDnsServiceListener = new ServiceListener() {
		public void serviceResolved(ServiceEvent ev) {
			System.out.println("serviceResolved");
			services.add(ev.getInfo());
			MainActivity.this.runOnUiThread(new Runnable() { 
					public void run() { 
						updateUi();
					}
			});
		}

		public void serviceRemoved(ServiceEvent ev) {
			ServiceInfo serviceToRemove = null;
			for(ServiceInfo info : services){
				if(info.getName().equals(ev.getName())){
					serviceToRemove = info;
					break;
				}
			}
			
			if(serviceToRemove != null){
				services.remove(serviceToRemove);
			}
		}

		public void serviceAdded(ServiceEvent event) {
			// Required to force serviceResolved to be called again
			// (after the first search)
			serviceFinder.requestServiceInfo(event.getType(), event.getName(), 1);
		}
	};


	private void updateUi() {
		StringBuilder text = new StringBuilder();
		
		for(ServiceInfo info: services){
			text.append(info.getName()).append("\n");
		}
		
		txtView.setText(text);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_PROGRESS);
		setContentView(R.layout.activity_main);
		services = new ArrayList<ServiceInfo>();
		txtView = (TextView) findViewById(R.id.mainTxt);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onResume() {
		super.onResume();
		WifiManager wifi = (WifiManager) getSystemService(android.content.Context.WIFI_SERVICE);
		lock = wifi.createMulticastLock("HeeereDnssdLock");
	    lock.setReferenceCounted(true);
	    lock.acquire();
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... arg0) {
				try {
					System.out.println("Starting serviceFinder");
					serviceFinder = JmDNS.create();
					System.out.println("Adding service listener");
					serviceFinder.addServiceListener("_http._tcp.local.", mDnsServiceListener);
				} catch (IOException e) {
					e.printStackTrace();
					System.out.println("oops");
				}
				return null;
			}
	    }.execute();
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		if (lock != null) lock.release();
		
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... arg0) {
				try {
					System.out.println("Stopping serviceFinder");
					serviceFinder.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return null;
			}
	    }.execute();
	}

}
