package com.example.musiclocal;

import java.io.File;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.http.Header;
import org.json.JSONArray;

import android.net.Uri;
import android.net.Uri.Builder;
import android.util.Log;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestHandle;

public class MusicLocalClient {
	private static final String MUSICS_PATH = "music";
	private static final String MUSICFILE_PATH = "file";
	private static final int MAX_RESPONSETIMEOUT = 60000 * 2; //2 min

/* ************************************* Singleton ******************************************** */
	private static MusicLocalClient singleton;
	public static MusicLocalClient instance() {
		if(singleton == null){
			singleton = new MusicLocalClient();
		}
		return singleton;
	}
	
/* ************************************** Interfaces ******************************************* */
	public interface OnGetMusicsListener{
		public void onGetMusics(List<Music> musics);
	}
	public interface OnMusicLoadedListener{
		public void onMusicLoad(MusicLoadResponse evt);
		public void onMusicLoadFailure(Music music, Throwable cause);
		public void onProgressLoading(Music music, float progress);
		public void onStartLoad(Music music);
	}

	public class MusicLoadResponse {
		private Music music;
		private File file;
		public MusicLoadResponse(Music music, File file){
			this.music = music;
			this.file = file;
		}
		public Music getMusic(){
			return music;
		}
		public File getFile(){
			return file;
		}
	}
	
/* ******************************************************************************************** */
	private MusicServiceInfo serviceInfo;
	private final List<OnGetMusicsListener> getMusicListeners;
	private final List<OnMusicLoadedListener> onMusicLoadedListeners;
	private AsyncHttpClient httpClient;
	private ObjectMapper jsonObjectMapper;
	
	public MusicLocalClient() {
		getMusicListeners = new ArrayList<MusicLocalClient.OnGetMusicsListener>();
		onMusicLoadedListeners = new ArrayList<MusicLocalClient.OnMusicLoadedListener>();
		httpClient = new AsyncHttpClient();
		jsonObjectMapper = new ObjectMapper();
		jsonObjectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
	}
/* ********************************** Getters and Setters ************************************* */
	public MusicServiceInfo getServiceInfo() {
		return this.serviceInfo;
	}
	public void setServiceInfo(MusicServiceInfo serviceInfo) {
		this.serviceInfo = serviceInfo;
	}

/* ************************************* Response Listeners *************************************** */
	public void addOnGetMusicsListener(OnGetMusicsListener listener){
		this.getMusicListeners.add(listener);
	}
	public void removeOnGetMusicsListener(OnGetMusicsListener listener){
		this.getMusicListeners.remove(listener);
	}
	public void notifyGetMusics(List<Music> musics){
		for(OnGetMusicsListener listener: this.getMusicListeners){
			listener.onGetMusics(musics);
		}
	}
	public void addOnMusicLoadedListener(OnMusicLoadedListener listener){
		this.onMusicLoadedListeners.add(listener);
	}
	public void removeOnMusicLoadedListener(OnMusicLoadedListener listener){
		this.onMusicLoadedListeners.remove(listener);
	}
	private void notifyMusicLoad(MusicLoadResponse musicLoadResponse) {
		for(OnMusicLoadedListener listener : this.onMusicLoadedListeners){
			listener.onMusicLoad(musicLoadResponse);
		}
	}
	protected void notifyMusicLoadFailure(Music music, Throwable cause) {
		for(OnMusicLoadedListener listener : this.onMusicLoadedListeners){
			listener.onMusicLoadFailure(music, cause);
		}
	}
/* ************************************* API connection *************************************** */
	public void getMusics() {
		String getUrl = generateGetMusicsUrl();
		
		Log.d(getClass().getName(), "GET " + getUrl);
		
		httpClient.get(getUrl, new JsonHttpResponseHandler(){
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
				Log.d(getClass().getName(), "Response:\n" + response);
				
				List<Music> musics = new ArrayList<Music>();
				for(int i=0; i < response.length(); ++i){
					Music music;
					try {
						music = jsonObjectMapper.readValue(response.getJSONObject(i).toString(), Music.class);
						musics.add(music);
					} catch (Exception e) { //TODO: repassar erros para listeners
						throw new RuntimeException("Could not parse json message:\n "
								+ "\"" + response + "\"",e);
					}
				}
				
				notifyGetMusics(musics);
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				throwable.printStackTrace();
			}
		});
		
	}
	public RequestHandle loadMusic(final Music music) {
		return loadMusic(music, null);
	}
	public RequestHandle loadMusic(final Music music, final OnMusicLoadedListener listener) {

		final File musicFile = getMusicFile(music);
		Log.d(getClass().getName(), "Generated music file: " + musicFile);
		if(musicFile.exists()){//Arquivo do cache
			musicLoadedSuccess(music, listener, musicFile);
		}
		else{
			return httpClient.get(generateLoadMusicUrl(music), new FileAsyncHttpResponseHandler(musicFile) {
				@Override
				public void onStart() {
					if(listener != null){
						listener.onStartLoad(music);
					}
				}
				@Override
				public void onSuccess(int arg0, Header[] arg1, File file) {
					Log.d(getClass().getName(), "Get music from network in file: " + file);
					musicLoadedSuccess(music, listener, file);
				}
				@Override
				public void onFailure(int arg0, Header[] arg1, Throwable cause, File arg3) {
					Log.d(getClass().getName(), "onFailure: music file = " + musicFile);
					Log.d(getClass().getName(), "onFailure: target file = " + getTargetFile());
					if(getTargetFile().exists()){
						boolean deleted = this.deleteTargetFile();
						Log.d(getClass().getName(), "Deleted target file? " + deleted);
					}
					if(cause instanceof SocketTimeoutException){
						Log.d(getClass().getName(), "Current Socket response timeout: " + httpClient.getResponseTimeout());
						if(httpClient.getResponseTimeout() < getMaxResponseTimeout()){
							httpClient.setResponseTimeout((int) (httpClient.getResponseTimeout() * 1.2f));
						}
					}
					notifyMusicLoadFailure(music, cause);
					if(listener != null){
						listener.onMusicLoadFailure(music, cause);
					}
				}
				@Override
				public void onProgress(int bytesWritten, int totalSize) {
					if(listener != null){
						listener.onProgressLoading(music, bytesWritten / (float)totalSize);
					}
				}
			});
		}
		return null;
	}

	protected int getMaxResponseTimeout() {
		return MAX_RESPONSETIMEOUT;
	}
	private void musicLoadedSuccess(final Music music,
			final OnMusicLoadedListener listener, File file) {
		MusicLoadResponse response = new MusicLoadResponse(music, file);
		notifyMusicLoad(response);
		if(listener != null){
			listener.onMusicLoad(response);
		}
	}
	
	private File getMusicFile(Music music) {
		File file = getCacheDir();
		String musicFilename = generateMusicFilename(music);
		return new File(file, musicFilename);
	}
	private String generateMusicFilename(Music music) {
		return this.serviceInfo.getServiceName() + "_" + String.valueOf(music.getId()) 
				+ FilenameUtils.getExtension(music.getFilename());
	}
	private File getCacheDir() {
		File externalCache =  MusicLocalClientApplication.instance().getExternalCacheDir();
		if(externalCache == null){
			return MusicLocalClientApplication.instance().getCacheDir();
		}
		return externalCache;
	}
/* ************************************* URL generation *************************************** */
	private String generateGetMusicsUrl() {
		return getBaseUri().appendPath(MUSICS_PATH).toString();
	}
	private String generateLoadMusicUrl(Music music) {
		return getBaseUri().appendPath(MUSICS_PATH)
						   .appendPath(String.valueOf(music.getId()))
						   .appendPath(MUSICFILE_PATH).toString();
	}

	private Builder getBaseUri() {
		int port = this.serviceInfo.getServicePort();
		try {
			return Uri.parse(new URL("http", serviceInfo.getServiceAdresses()[0],port,"").toString()).buildUpon();
		} catch (MalformedURLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
}
