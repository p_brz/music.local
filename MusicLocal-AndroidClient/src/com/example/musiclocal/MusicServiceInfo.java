package com.example.musiclocal;

import java.io.Serializable;

public class MusicServiceInfo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6267351144200505987L;

	private String serviceName;
	private String[] serviceAdresses;
	private int servicePort;
	
	public MusicServiceInfo(String serviceName, int servicePort, String[] serviceAdresses) {
		super();
		this.serviceName = serviceName;
		this.serviceAdresses = serviceAdresses;
		this.servicePort = servicePort;
	}
	
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String[] getServiceAdresses() {
		return serviceAdresses;
	}
	public void setServiceAdresses(String[] serviceAdresses) {
		this.serviceAdresses = serviceAdresses;
	}
	public int getServicePort() {
		return servicePort;
	}
	public void setServicePort(int servicePort) {
		this.servicePort = servicePort;
	}
}
