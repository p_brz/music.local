package com.example.musiclocal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.musiclocal.MusicLocalClient.OnGetMusicsListener;

public class ListMusicsActivity extends ActionBarActivity 
						implements OnGetMusicsListener, OnItemClickListener, ServiceConnection 
{	
	private final List<Music> musics;
	private MusicArrayAdapter musicsAdapter;
	private ListView musicsListView;
	private MusicPlayerBinder musicPlayerBinder;
	private final MusicPlayerServiceReceiver serviceReceiver;
	private ImageButton togglePlayButton, playNextButton, playPreviousButton;
	
	public ListMusicsActivity() {
		musics = new ArrayList<Music>();
		serviceReceiver = new MusicPlayerServiceReceiver();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_musics);
		
		setupView();
	}
	@Override
	protected void onResume() {
		super.onResume();
		registerListeners();
		MusicServiceInfo servInfo = MusicLocalClient.instance().getServiceInfo();
		Log.d(getClass().getName(), "Service info: " + servInfo.getServiceName() 
									+ "; " + servInfo.getServicePort()
									+ "; " + servInfo.getServiceAdresses());
		MusicLocalClient.instance().getMusics();
		
		bindToService();
	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterListeners();
		unbindToService();
	}
	
	
/* ************************************************* Player actions ******************************************************************/
	private void togglePlay() {
		if(musicPlayerBinder != null){
			musicPlayerBinder.togglePlay();
		}
	}
	private void playNext() {
		if(musicPlayerBinder != null){
			musicPlayerBinder.playNext();
		}
	}

	private void playPrevious() {
		if(musicPlayerBinder != null){
			musicPlayerBinder.playPrevious();
		}
	}
//	private void close() {
//		if(musicPlayerBinder != null){
//			musicPlayerBinder.finish();
//		}
//		this.finish();
//	}

/* ************************************************* Views ******************************************************************/
	private void setupView() {
//		musicsAdapter = new MusicArrayAdapter(this, android.R.layout.simple_list_item_1);
		musicsAdapter = new MusicArrayAdapter(this, R.layout.listitem_music, R.id.musicListItem_text);
		musicsListView = (ListView)findViewById(R.id.listMusicsView);
		musicsListView.setAdapter(musicsAdapter);
		musicsListView.setOnItemClickListener(this);

		togglePlayButton   = (ImageButton)findViewById(R.id.musicToolbar_PlayButton);
		playPreviousButton = (ImageButton)findViewById(R.id.musicToolbar_PreviousButton);
		playNextButton     = (ImageButton)findViewById(R.id.musicToolbar_NextButton);
		togglePlayButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				togglePlay();
			}
		});
		playPreviousButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				playPrevious();
			}
		});
		playNextButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				playNext();
			}
		});

	}

/* ******************************************* Listeners ******************************************************/
	@Override
	public void onGetMusics(List<Music> musics) {
		this.musicsAdapter.clear();
		this.musics.clear();
		for(Music music : musics){
			this.musicsAdapter.add(music);
			this.musics.add(music);
		}
		if(this.musics.size() > 0){
			int testSelectionPos = musicsAdapter.getCount()/2;
			this.musicsListView.setItemChecked(testSelectionPos, true);;
			this.musicsListView.smoothScrollToPosition(testSelectionPos);
		}
	}
	
	private void registerListeners() {
		MusicLocalClient.instance().addOnGetMusicsListener(this);
		serviceReceiver.register(this);
	}
	private void unregisterListeners() {
		MusicLocalClient.instance().removeOnGetMusicsListener(this);
		serviceReceiver.unregister(this);
	}

	/* ************************************ OnItemClickListener ***********************************************/
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if(parent == musicsListView){
//			Music music = this.musicsAdapter.getItem(position);
//			playMusic(music);
			playAll(position);
		}
	}


	private void playAll(int startIndex) {
		Intent playMusicIntent = new Intent(getApplicationContext(), MusicPlayerService.class);
		playMusicIntent.setAction(MusicPlayerService.ACTION_PLAY);
		playMusicIntent.putExtra(MusicPlayerService.EXTRA_PLAYLIST, (Serializable)this.musics);
		playMusicIntent.putExtra(MusicPlayerService.EXTRA_PLAYLIST_STARTINDEX, startIndex);
		
		startService(playMusicIntent);
	}


/* ******************************************* Service binding ******************************************************/
	private void bindToService() {
		// Bind to LocalService
        Intent intent = new Intent(this, MusicPlayerService.class);
        bindService(intent, this, Context.BIND_AUTO_CREATE);
	}
	private void unbindToService() {
        if (musicPlayerBinder != null) {
            unbindService(this);
        }
	}
	/* ***************************************** ServiceConnection ********************************************/
	@Override
	public void onServiceConnected(ComponentName name, IBinder binder) {
		this.musicPlayerBinder = (MusicPlayerBinder) binder;
		Log.d(getClass().getName(), "Bound to service!");
	}

	@Override
	public void onServiceDisconnected(ComponentName name) {
		Log.d(getClass().getName(), "Service disconnected");
		this.musicPlayerBinder = null;
	}
	/* ********************************************************************************************************/
	public class MusicArrayAdapter extends ArrayAdapter<Music>{
		long highlightedItem;
		
		int textViewResourceId;
		public MusicArrayAdapter(Context context, int resource) {
			super(context, resource);
			init(0);
		}
		public MusicArrayAdapter(Context context, int resource,
				int textViewResourceId) {
			super(context, resource, textViewResourceId);
			init(textViewResourceId);
		}
		private final void init(int textViewResourceId){
			this.textViewResourceId = textViewResourceId;
			this.highlightedItem = -1;			
		}

		@Override
		public long getItemId(int position) {
			return super.getItem(position).getId();
		}
		
		@SuppressWarnings("deprecation")
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Log.d(getClass().getName(), "getView position: " + position);
			View view =  super.getView(position, convertView, parent);

			Log.d(getClass().getName(), "getView: view id: " + view.getId());
			
			Music music = getItem(position);
			view.setId((int) music.getId());
			
			TextView textView;
			if(textViewResourceId == 0){
				textView = ((TextView)view);
			}
			else{
				textView=(TextView)view.findViewById(textViewResourceId);
			}

			if(convertView == null){//criou view agora
				textView.setTag(view.getBackground());
			}
			
			if(highlightedItem == music.getId()){
				textView.setBackgroundResource(R.drawable.abc_list_longpressed_holo);
			}
			else{
				textView.setBackgroundDrawable((Drawable)textView.getTag());
			}

			textView.setText(music.getFilename());
			
			return view;
		}
		
		public void highlightMusic(Music music){
			int musicIndex = findMusic(music);
			if(musicIndex > 0){
				highlightedItem = music.getId();
			}
			else{
				highlightedItem = -1;
			}
			this.notifyDataSetChanged();
		}
		private int findMusic(Music music) {
			if(music == null){
				return -1;
			}
			for(int i=0; i < musics.size(); ++i){
				if(musics.get(i).getId() == music.getId()){
					return i;
				}
			}
			return -1;
		}
		public void clearHighlight() {
			highlightedItem = -1;
			this.notifyDataSetChanged();
		}
		
		public View findView(AdapterView<? extends Adapter> parentView, Music music){
			return parentView.findViewById((int)music.getId());
		}
	}
	/* ********************************************************************************************************/
	public class MusicPlayerServiceReceiver extends BroadcastReceiver{
		public void register(Context context){
			LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
			broadcastManager.registerReceiver(this, new IntentFilter(MusicPlayerService.ACTION_ONPAUSE));
			broadcastManager.registerReceiver(this, new IntentFilter(MusicPlayerService.ACTION_ONRESUME));
			broadcastManager.registerReceiver(this, new IntentFilter(MusicPlayerService.ACTION_ONSTART));
			broadcastManager.registerReceiver(this, new IntentFilter(MusicPlayerService.ACTION_ONSTOP));
			broadcastManager.registerReceiver(this, new IntentFilter(MusicPlayerService.ACTION_ONFINISH));

			broadcastManager.registerReceiver(this, new IntentFilter(MusicPlayerService.ACTION_ONLOADFAILURE));
			broadcastManager.registerReceiver(this, new IntentFilter(MusicPlayerService.ACTION_ONSTARTLOAD));
			broadcastManager.registerReceiver(this, new IntentFilter(MusicPlayerService.ACTION_ONPROGRESSLOADING));
		}
		public void unregister(Context context){
			LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
			broadcastManager.unregisterReceiver(this);
		}
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if(action != null){
				Music music = (Music)intent.getSerializableExtra(MusicPlayerService.EXTRA_MUSIC);
				if(MusicPlayerService.ACTION_ONRESUME.equals(action)){
					onResumeMusic(music);
				}
				else if(MusicPlayerService.ACTION_ONPAUSE.equals(action)){
					onPauseMusic(music);
				}
				else if(MusicPlayerService.ACTION_ONSTART.equals(action)){
					onStartMusic(music);
				}
				else if(MusicPlayerService.ACTION_ONSTOP.equals(action)){
					onStopMusic(music);
				}
				else if(MusicPlayerService.ACTION_ONFINISH.equals(action)){
					onFinishService();
				}
				else if(MusicPlayerService.ACTION_ONLOADFAILURE.equals(action)){
					Throwable cause = (Throwable) intent.getSerializableExtra(MusicPlayerService.EXTRA_FAILURECAUSE);
					onLoadFailure(music, cause);
				}
				else if(MusicPlayerService.ACTION_ONSTARTLOAD.equals(action)){
					onStartLoad(music);
				}
				else if(MusicPlayerService.ACTION_ONPROGRESSLOADING.equals(action)){
					float progress = intent.getFloatExtra(MusicPlayerService.EXTRA_PROGRESS, -1);
					onLoadProgress(music, progress);
				}
			}
		}
	}
	

	public void onStartMusic(Music music) {
		updateMusicController();
		musicsAdapter.highlightMusic(music);
	}

	public void onStopMusic(Music music) {
		updateMusicController();
		musicsAdapter.highlightMusic(null);
	}
	public void onResumeMusic(Music music) {
		updateMusicController();
	}
	public void onPauseMusic(Music music) {
		updateMusicController();
	}
	public void onFinishService() {
	}
	public void onLoadProgress(Music music, float progress) {
		Log.d(getClass().getName(), "onLoadProgress: " + music.getFilename() + "; " + progress + "%");
		ProgressBar progressBar = getProgressBar(music);
		if(progressBar != null){
			progressBar.setProgress((int) (progress * 100));
			if(progress == 1.0f){
				progressBar.setVisibility(View.GONE);
			}
		}
	}
	public void onStartLoad(Music music) {
		Log.d(getClass().getName(), "start load: " + music.getFilename());
		ProgressBar progressBar = getProgressBar(music);
		if(progressBar != null){
			progressBar.setVisibility(View.VISIBLE);
			progressBar.setProgress(0);
		}
	}

	public void onLoadFailure(Music music, Throwable cause) {
		Log.e(getClass().getName(), "Failed when loading music. \"" + music.getFilename() + "\"",cause);
		ProgressBar progressBar = getProgressBar(music);
		if(progressBar != null){
			progressBar.setVisibility(View.GONE);
		}
		
		Toast.makeText(this, "Ocorreu um erro ao tentar carregar : \"" + music.getFilename() + "\""
				, Toast.LENGTH_LONG).show();
	}

	private ProgressBar getProgressBar(Music music) {
		View view = this.musicsAdapter.findView(this.musicsListView, music);
		if(view != null){
			return (ProgressBar) view.findViewById(R.id.musicListItem_progress);
		}
		return null;
	}
	private void updateMusicController() {
		if(musicPlayerBinder != null && musicPlayerBinder.isPlaying()){
			togglePlayButton.setImageResource(R.drawable.ic_action_pause);
		}
		else{
			togglePlayButton.setImageResource(R.drawable.ic_action_play);
		}

		togglePlayButton.setEnabled(musicPlayerBinder != null);
		playPreviousButton.setEnabled(musicPlayerBinder != null && musicPlayerBinder.hasPrevious());
		playNextButton.setEnabled(musicPlayerBinder != null && musicPlayerBinder.hasNext());
	}
}
