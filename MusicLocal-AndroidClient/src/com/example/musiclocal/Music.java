package com.example.musiclocal;

import java.io.Serializable;

public class Music implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2712071932820866095L;
	
	private long id;
	private String filename;

	public Music() {
		this(0, "");
	}
	public Music(long id, String name) {
		super();
		this.id = id;
		this.filename = name;
	}
	public Music(Music currentMusic) {
		this(currentMusic.getId(), currentMusic.getFilename());
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
}
