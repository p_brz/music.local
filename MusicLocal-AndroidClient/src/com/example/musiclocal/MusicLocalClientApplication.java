package com.example.musiclocal;

import android.app.Application;

public class MusicLocalClientApplication extends Application{
	private static MusicLocalClientApplication globalApplication;
	public static MusicLocalClientApplication instance(){
		return globalApplication;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		if(globalApplication == null){
			globalApplication = this;
		}
	}
	
}
