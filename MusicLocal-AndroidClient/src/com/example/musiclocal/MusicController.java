package com.example.musiclocal;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.MediaController;

public class MusicController extends MediaController {
	
	private boolean alwaysVisible;
	
	public MusicController(Context context) {
		super(context);
		init();
	}
	public MusicController(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	public MusicController(Context context, boolean useFastForward) {
		super(context, useFastForward);
		init();
	}

	private final void init() {
		alwaysVisible = false;
	}
	
	@Override
	public void hide() {
		if(!alwaysVisible){
			super.hide();
		}
	}
	
	@Override
    public boolean dispatchKeyEvent(KeyEvent event) {

        if(event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
        	if(isShown()){
        		this.forceHide();
        	}
            return false;
        }
        return super.dispatchKeyEvent(event);
    }
	
	public void forceHide(){
		super.hide();
	}
	
	public boolean isAlwaysVisible() {
		return alwaysVisible;
	}
	public void setAlwaysVisible(boolean alwaysVisible) {
		this.alwaysVisible = alwaysVisible;
	}
	
	
}
