package com.example.musiclocal;

import java.util.HashMap;
import java.util.Map;

import android.app.Notification;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.musiclocal.MusicLocalClient.MusicLoadResponse;
import com.example.musiclocal.MusicLocalClient.OnMusicLoadedListener;
import com.example.musiclocal.MusicPlayer.PlayerListener;
import com.example.musiclocal.commands.CommandExecutor;
import com.example.musiclocal.commands.NextMusicCommand;
import com.example.musiclocal.commands.PlayCommand;
import com.example.musiclocal.commands.PlayPauseCommand;
import com.example.musiclocal.commands.PreviousMusicCommand;

public class MusicPlayerService extends LongLivedIntentService implements PlayerListener{
	private static final String CLASS 		 = MusicPlayerService.class.getName();
	private static final String ACTIONS 	 = CLASS + ".ACTIONS";
	public static final String ACTION_PLAY   	  = ACTIONS + ".PLAY";
	public static final String ACTION_RESUME   	  = ACTIONS + ".RESUME";
	public static final String ACTION_PAUSE   	  = ACTIONS + ".PAUSE";
	public static final String ACTION_TOGGLEPLAY  = ACTIONS + ".TOGGLEPLAY";
	public static final String ACTION_FINISH      = ACTIONS + ".FINISH";
	public static final String ACTION_PREVIOUS    = ACTIONS + ".PREVIOUS";
	public static final String ACTION_NEXT        = ACTIONS + ".NEXT";

	public static final String ACTION_ONRESUME = ACTIONS + ".ONRESUME";
	public static final String ACTION_ONPAUSE  = ACTIONS + ".ONPAUSE";
	public static final String ACTION_ONSTART  = ACTIONS + ".ONSTART";
	public static final String ACTION_ONSTOP   = ACTIONS + ".ONSTOP";
	public static final String ACTION_ONFINISH = ACTIONS + ".ONFINISH";
	public static final String ACTION_ONPROGRESSLOADING = ACTIONS + ".ONPROGRESSLOADING";
	public static final String ACTION_ONSTARTLOAD       = ACTIONS + ".ONSTARTLOAD";
	public static final String ACTION_ONLOADFAILURE     = ACTIONS + ".ONLOADFAILURE";

	private static final String EXTRAS 	      = CLASS + ".EXTRAS";
	public static final String EXTRA_MUSIC    = EXTRAS + ".MUSIC";
	public static final String EXTRA_PLAYLIST = EXTRAS + ".PLAYLIST";
	public static final String EXTRA_PLAYLIST_STARTINDEX = EXTRAS + ".PLAYLIST_STARTINDEX";
	public static final String EXTRA_PROGRESS     = EXTRAS + ".PROGRESS";
	public static final String EXTRA_FAILURECAUSE = EXTRAS + ".FAILURE_CAUSE";
	
	private static final int FOREGROUND_NOTIFICATION_ID = 1;
	private final MusicPlayer musicPlayer;	
	private final Map<String, CommandExecutor> commands;
	private MusicPlayerBinder musicPlayerBinder;
	private boolean foregroundServiceEnabled;
	private MusicLoadBroadcaster musicLoadBroadcaster;

/* ************************************** Service Creation ********************************************** */
	public MusicPlayerService() {
		musicPlayer = new MusicPlayer();
		musicPlayer.addPlayerListener(this);
		musicLoadBroadcaster = new MusicLoadBroadcaster();
		musicPlayer.setMusicLoadedListener(musicLoadBroadcaster);
		musicPlayerBinder = new MusicPlayerBinder(this, musicPlayer);
		
		commands = new HashMap<String, CommandExecutor>();
		setupCommands(commands);
		foregroundServiceEnabled = true;
	}
	
	private void setupCommands(Map<String, CommandExecutor> commandsMap) {
		commandsMap.put(ACTION_PLAY      , new PlayCommand(this.musicPlayer));
		PlayPauseCommand playPauseCommand = new PlayPauseCommand(this.musicPlayer);
		commandsMap.put(ACTION_TOGGLEPLAY, playPauseCommand);
		commandsMap.put(ACTION_RESUME    , playPauseCommand);
		commandsMap.put(ACTION_PAUSE     , playPauseCommand);
		commandsMap.put(ACTION_NEXT      , new NextMusicCommand(this.musicPlayer));
		commandsMap.put(ACTION_PREVIOUS  , new PreviousMusicCommand(this.musicPlayer));
	}

/* **************************************** Getters and Setters *************************************** */
	public Music getCurrentMusic() {
		return this.musicPlayer.getCurrentMusic();
	}
	public boolean isPlaying() {
		return this.musicPlayer.isPlaying();
	}
	/* **************************************************************************************************** */
	public void hideForegroundService() {
		stopForeground(true);
		foregroundServiceEnabled = false;
	}
	public void showForegroundService() {
		foregroundServiceEnabled = true;
		updateNotificationStatus();
	}
	private void updateNotificationStatus() {
		if(!foregroundServiceEnabled){
			return;
		}
		Notification notif = ForegroundServiceCreator.instance()
				.createForegroundNotification(this);
		startForeground(FOREGROUND_NOTIFICATION_ID, notif);
	}
/* **************************************************************************************************** */
	@Override
	protected void onHandleIntent(Intent intent, int startId) {
		if(intent != null){
			String action = intent.getAction();
			Log.d(getClass().getName(), "Received action: " + action);
			if(action != null){
				if(ACTION_FINISH.equals(action)){
					finishService();
				}
				else if(commands.containsKey(action)){
					commands.get(action).execute(intent, startId);
				}
			}
		}
	}

	public void finishService() {
		this.musicPlayer.stop();
		broadcast(null, ACTION_ONFINISH);
		stopForeground(true);
		stopSelf();
	}

	@Override
	public IBinder onBind(Intent intent) {
		return this.musicPlayerBinder;
	}
/* **************************************** PlayerListener ****************************************** */
	@Override
	public void onStart(Music music) {
		updateNotificationStatus();
		broadcast(music, ACTION_ONSTART);
	}
	@Override
	public void onResume(Music music) {
		updateNotificationStatus();
		broadcast(music, ACTION_ONRESUME);
	}
	@Override
	public void onPause(Music music) {
		updateNotificationStatus();
		broadcast(music, ACTION_ONPAUSE);
	}
	@Override
	public void onStop(Music music) {
		broadcast(music, ACTION_ONSTOP);
	}
	private void broadcast(Music music, String action) {
		broadcast(new Intent(), music, action);
	}
	private void broadcast(Intent intent, Music music, String action) {
		intent.setAction(action);
		if(music != null){
			intent.putExtra(EXTRA_MUSIC, new Music(music));
		}
		LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
	}

/* **************************************** MusicLoadListener ****************************************** */

	class MusicLoadBroadcaster implements OnMusicLoadedListener{
		protected static final float NOTIFYPROGRESS_THRESHOLD = 10f;
		protected static final long NOTIFYPROGRESS_TIMETHRESHOLD = 1000;
		private float lastProgress = 0;
		private long startTimeMillis;
		@Override
		public void onMusicLoad(MusicLoadResponse response) {
			broadcastMusicLoaded(response);
		}
		@Override
		public void onMusicLoadFailure(Music music, Throwable cause) {
			broadcastMusicLoadFailure(music, cause);
		}
		@Override
		public void onStartLoad(Music music) {
			lastProgress = 0;
			startTimeMillis = System.currentTimeMillis(); 
			broadcastMusicStartLoad(music);
		}

		@Override
		public void onProgressLoading(Music music, float progress) {
			// TODO Auto-generated method stub
			Log.d(getClass().getName(), "Progress loading \"" + music.getFilename() + "\" : " + progress);
			if(progress - lastProgress > NOTIFYPROGRESS_THRESHOLD
					||reachTimeThreshold())
			{
				lastProgress = progress;
				broadcastMusicLoadingProgress(music, progress);
			}
		}

		private void broadcastMusicStartLoad(Music music) {
			broadcast(music, ACTION_ONSTARTLOAD);
		}
		private void broadcastMusicLoadingProgress(Music music, float progress) {
			Intent intent = new Intent();
			intent.putExtra(EXTRA_PROGRESS, progress);
			broadcast(intent, music, ACTION_ONPROGRESSLOADING);
		}
		private void broadcastMusicLoaded(MusicLoadResponse response) {
			// ... ()
		}
		private void broadcastMusicLoadFailure(Music music, Throwable cause) {
			Intent intent = new Intent();
			intent.putExtra(EXTRA_FAILURECAUSE, cause);
			broadcast(intent, music, ACTION_ONLOADFAILURE);
		}
		private boolean reachTimeThreshold() {
			return System.currentTimeMillis() - startTimeMillis > NOTIFYPROGRESS_TIMETHRESHOLD;
		}
	}
}
