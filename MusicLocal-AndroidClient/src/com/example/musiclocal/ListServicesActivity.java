package com.example.musiclocal;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceEvent;
import javax.jmdns.ServiceInfo;
import javax.jmdns.ServiceListener;

import android.content.Intent;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.MulticastLock;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;

public class ListServicesActivity extends ActionBarActivity implements OnItemClickListener {
    private static final String SERVICE_TYPE = "_musiclocal._tcp.local.";
	private static final String PACKAGE_NAME = DnssdSampleDiscovery.class.getPackage().getName() ;
    private static final String MULTICAST_LOCK_TAG = PACKAGE_NAME + ".MULTICAST_LOCK" ;
    
    private MulticastLock lock;
    private Handler handler = new android.os.Handler();
    
    private JmDNS jmdns;
    private ServiceListener serviceListener;
	
	private Map<String, ServiceInfo> services;
	private ArrayAdapter<String> servicesAdapter;
	private ListView servicesView;
	private ProgressBar waitServiceProgressBar;
	
	/* **************************************** Life Cycle ******************************************** */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_services);
		services = new HashMap<String, ServiceInfo>();
		
		setupView();
        setupServiceDiscovery();
	}
    
    @Override
    protected void onResume() {
    	super.onResume();
    	startServiceBrowser();
    }
	@Override
    protected void onPause() {
    	super.onPause();
    	stopServiceBrowser();
    }

	private void setupServiceDiscovery() {
		serviceListener = new MusicServiceListener();
	}
	private void setupView() {
		servicesAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1);		
		servicesView = (ListView)findViewById(R.id.listServicesView);
		servicesView.setAdapter(servicesAdapter);
		servicesView.setOnItemClickListener(this);
		
		this.waitServiceProgressBar = (ProgressBar) findViewById(R.id.progress_waitService);
	}
	/* **************************************** Multicast Lock ******************************************** */
	
	private MulticastLock acquireLock() {
		WifiManager wifiManager = (WifiManager)getSystemService(WIFI_SERVICE);
		MulticastLock lock =wifiManager.createMulticastLock(MULTICAST_LOCK_TAG);
		
		lock.setReferenceCounted(true);
		lock.acquire();
		
		return lock;
	}
    private void releaseLock() {
    	if(lock != null){
    		lock.release();
    		lock = null;
    	}
	}

	/* **************************************** ProgressBar ******************************************** */
	private void showServiceWaitingIndicator(){
		this.waitServiceProgressBar.setVisibility(View.VISIBLE);
	}
	private void hideServiceWaitingIndicator(){
		this.waitServiceProgressBar.setVisibility(View.GONE);
	}
	/* ************************************** OnItemClickListener ***************************************** */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		String serviceName = this.servicesAdapter.getItem(position);
		ServiceInfo serviceInfo = this.services.get(serviceName);
		startMusicService(serviceInfo);
	}

	private void startMusicService(ServiceInfo serviceInfo) {
		Intent listMusicIntent = new Intent(this, ListMusicsActivity.class);
		MusicServiceInfo musicServiceInfo = new MusicServiceInfo(
				serviceInfo.getName(), serviceInfo.getPort(), serviceInfo.getHostAddresses());
		
		MusicLocalClient.instance().setServiceInfo(musicServiceInfo);
		startActivity(listMusicIntent);
	}
	/* ****************************************Service browsing ******************************************** */
	protected void startServiceBrowser() {
		this.lock = acquireLock();
		initializeServiceBrowsingAsync();
	}

	protected void stopServiceBrowser() {
    	releaseLock();
    	finishServiceBrowsingAsync();
	}
	
	private void initializeServiceBrowsingAsync() {
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				showServiceWaitingIndicator();
			}
			@Override
			protected Void doInBackground(Void... arg0) {
				log("prepare to create");
				try {
					InetAddress hostAddress = getHostAddress();
					createJmdns(hostAddress);
					browseMusicServices();
				} catch (IOException e) {
					log("Failed starting service browsing.");
					e.printStackTrace();
				}
				return null;
			}
			private InetAddress getHostAddress() {
				Collection<InetAddress> ips = NetworkUtils.getIPAddresses();
				InetAddress hostAddress = null;
				List<InetAddress> ipv6Adresses = new LinkedList<InetAddress>();
				for(InetAddress addr: ips){
					if(addr instanceof Inet4Address){
						hostAddress = addr;
						break;
					}
					else{
						ipv6Adresses.add(addr);
					}
				}
				if(hostAddress == null && ipv6Adresses.isEmpty() == false){
					hostAddress = ipv6Adresses.get(0);
				}
				
				Log.d(getClass().getName(), "Host address: " + hostAddress);
				return hostAddress;
			}
			private void createJmdns(InetAddress hostAddress)
					throws IOException {
				if(hostAddress != null){
					jmdns = JmDNS.create(hostAddress);
				}
				else{
					jmdns = JmDNS.create();
				}
				log("created at hostname: " + jmdns.getHostName());
				log("in interface: " + jmdns.getInterface());
			}
			private void browseMusicServices() {
				jmdns.addServiceListener(SERVICE_TYPE, serviceListener);
				
				ServiceInfo services[] = jmdns.list(SERVICE_TYPE);
				
				for(ServiceInfo info: services){
					log("Found: " + info.getName());
					if(info.getHostAddresses().length == 0){
						jmdns.requestServiceInfo(info.getType(), info.getName());
					}
					else{
						putServiceFromOtherThread(info);
					}
				}
				
				if(services.length == 0){
					log("Not found any service of type: " + SERVICE_TYPE);
				}
			}
	    }.execute();
	}

	private void finishServiceBrowsingAsync() {
		if(jmdns != null){
			new AsyncTask<Void, Void, Void>() {
				@Override
				protected Void doInBackground(Void... arg0) {
					try {
						Log.d(getClass().getName(), "Stopping service discovery");
						jmdns.close();
						Log.d(getClass().getName(), "Finish service discovery");
					} catch (IOException e) {
						e.printStackTrace();
					}
					return null;
				}
		    }.execute();
		}
	}
	private void log(final String text) {
		Log.d(getClass().getName(), text);
	}
	/* **************************************** ServiceListener ******************************************* */
    class MusicServiceListener implements ServiceListener{

		@Override
		public void serviceAdded(ServiceEvent servEvt) {
			Log.d(getClass().getName(), 
					"In Thread: " + Thread.currentThread().getName() + "; " + 
					"Added Service \"" + servEvt.getName() +"\" : " + servEvt.getInfo());
			
			jmdns.requestServiceInfo(servEvt.getType(), servEvt.getName());
		}
		@Override
		public void serviceRemoved(ServiceEvent servEvt) {
			final String serviceName = servEvt.getName();
			Log.d(getClass().getName(), 
					"In Thread: " + Thread.currentThread().getName() + "; " + 
					"Remove Service \"" + serviceName +"\"");
			
			removeServiceFromOtherThread(serviceName);
		}
		@Override
		public void serviceResolved(ServiceEvent servEvt) {
			Log.d(getClass().getName(), 
					"In Thread: " + Thread.currentThread().getName() + "; " + 
					"Resolved Service \"" + servEvt.getName() +"\" : " + servEvt.getInfo());

			putServiceFromOtherThread(servEvt.getInfo());
		}
    }
    
    /* ******************************* Service List Controller *************************************** */
	private void putServiceFromOtherThread(final ServiceInfo info) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				putService(info);
			}
		});
	}
	private void removeServiceFromOtherThread(final String serviceName) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				removeService(serviceName);
			}
		});
	}
	private void putService(ServiceInfo serviceInfo) {
		hideServiceWaitingIndicator();
		
		String serviceName = serviceInfo.getName();
		removeService(serviceName);

		services.put(serviceName, serviceInfo);
		servicesAdapter.add(serviceName);
	}
	private void removeService(String serviceName) {
		if(services.containsKey(serviceName)){
			servicesAdapter.remove(serviceName);
		}
	}
}
